import * as React from 'react'
import { BackHandler } from 'react-native'
import { Provider } from 'react-redux'
import AppWithNavigationState from './navigators/AppNavigator'
import store from './redux/Store'
import { NavigationActions } from 'react-navigation';

interface Props {
	dispatch: any
	nav: any
}

export default class App extends React.Component<Props, undefined> {
	componentDidMount(){
		BackHandler.addEventListener('hardwareBackPress', this.onBackPress)
	}
	componentWillUnmount(){
		BackHandler.removeEventListener('hardwareBackPress', this.onBackPress)
	}
	onBackPress = () => {
		const { dispatch, nav } = this.props
		if (nav.index === 0) {
			return false
		}
		dispatch(NavigationActions.back())
		return true
	}
  render(){
    return(
      <Provider store={store}>
        <AppWithNavigationState />
      </Provider>
    )
  }
}

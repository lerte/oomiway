import * as React from 'react'
import { View } from 'react-native'

import {BasicHeader} from '../common/Settings'
import FooterButton from '../common/UIFooterButton'
import SelectAddressListView from '../main/SelectAddress/AddressListView'

import { NavigationAction, NavigationScreenProp } from 'react-navigation'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
}

interface State {
    selectedRow: number
}

export default class AddressListViewController extends React.Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {selectedRow: null}
    }
    static navigationOptions = ({ navigation }) => BasicHeader(navigation, 'Address List')

    _getSelectedRow(row) {
        this.setState({selectedRow: row})
    }

    render() {
        const { navigate, goBack } = this.props.navigation
        return (
            <View style={{height: '100%'}}>
                <SelectAddressListView
                    onAddButtonPress={() => navigate('AddAddress')}
                    onEditBtnPress={() => navigate('AddAddress', {edit: this.state.selectedRow})}
                    onRowPress={this._getSelectedRow.bind(this)}
                ></SelectAddressListView>
                <FooterButton
                    title="Confirm"
                    onPress={() => goBack()}
                ></FooterButton>
            </View>
        )
    }
}
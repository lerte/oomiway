import * as React from 'react'
import { View, StyleSheet, Text, Image, TouchableOpacity, ImageBackground } from 'react-native'

interface Props {
    user: string
    image: any
    onPress: any
}

export default class AccountPicture extends React.Component<Props, undefined> {
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground style={styles.imgWrap} source={this.props.image}>
                    <View style={styles.overlay}></View>
                    <TouchableOpacity
                        onPress={this.props.onPress}
                    >
                        <Image
                            style={styles.smallPic}
                            source={this.props.image}
                        ></Image>
                    </TouchableOpacity>
                    <Text style={styles.accountName}>{this.props.user}</Text>
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        overflow: 'hidden',
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4
    },
    imgWrap: {
        height: 170,
        width: '100%',
        alignItems: 'center'
    },
    overlay: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundColor: '#f26f21',
        opacity: 0.85
    },
    smallPic: {
        height: 70,
        width: 70,
        borderRadius: 35,
        marginTop: 25,
        marginBottom: 20
    },
    accountName: {
        color: 'white',
        fontSize: 18,
        backgroundColor: 'transparent'
    }
})
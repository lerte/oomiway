import * as React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

import TextStyles from '../../common/TextStyles'
import { IconChevronRightSvg } from '../../common/IconsSvg'

interface Props {
    title: string
    message: string
    icon: any
    onPress?: any
}

export default class AccountInfoNav extends React.Component<Props, undefined> {
    render() {
        return (
            <TouchableOpacity
                style={styles.container}
                onPress={this.props.onPress}
            >
                <View style={styles.body}>
                    <View style={styles.info}>
                        <View style={styles.icon}>{this.props.icon}</View>
                        <Text style={[TextStyles.lightReg, styles.text]}>{this.props.title}:
                            <Text style={TextStyles.regText}> {this.props.message}</Text>
                        </Text>
                    </View>
                    <IconChevronRightSvg></IconChevronRightSvg>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        borderBottomWidth: 0.5,
        borderColor: '#efefef',
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        height: 62
    },
    body: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        flex: 1
    },
    info: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    icon: {
        marginRight: 15
    },
    text: {
        width: '80%'
    }
})
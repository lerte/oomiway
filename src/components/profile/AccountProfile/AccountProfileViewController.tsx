import * as React from 'react'
import { View, StyleSheet, Text } from 'react-native'

import AccountInfoNav from './AccountInfoNav'
import AccountPicture from './AccountPicture'

import { IconEmailSvg, IconPaymentMethodSvg, IconPhoneSvg, IconWaypointSvg } from '../../common/IconsSvg'
import { BasicHeader, Logout } from '../../common/Settings'

import { NavigationAction, NavigationScreenProp } from 'react-navigation'
import ActionSheet from 'react-native-actionsheet'
import ImagePicker from 'react-native-image-crop-picker'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
}

interface State{
    image: any
}

export enum Mode {
    email,
    phone
}

const actionSheetButtons = ['Camera', 'Select from album', 'Cancel']


export default class AccountProfileViewController extends React.Component<Props, State> {
    static navigationOptions = ( {navigation} ) => BasicHeader(navigation, 'Account Profile', Logout(navigation))
    constructor(props) {
        super(props)
        this.state = { image: require('../../../../images/shrek.jpg') }
    }
    private ActionSheet = {
        show: ()=>{}
    }

    _handleActionSheetPress(i:number){
        switch(i){
            case 0: { //Camera
                ImagePicker.openCamera({
                    width: 360,
                    height: 360,
                    cropping: true
                }).then(image => {
                    this._updateImage(image)
                }).catch(e=>{
                    console.log(e)
                })
                break
            }
            case 1: { //Select from album
                ImagePicker.openPicker({
                    width: 360,
                    height: 360,
                    mediaType: 'photo',
                    cropperCircleOverlay: true,
                    cropping: true
                }).then(image => {
                    this._updateImage(image)
                }).catch(e=>{
                    console.log(e)
                })
                break
            }
            case 3: { //Cancel
                break
            }
            default: {
                break
            }
        }
    }

    _updateImage(image) {
        this.setState({
            image: {uri: image['path'], width: image['width'], height: image['height'], mime: image['mime']}
        })
    }

    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <AccountPicture
                    user="Shrek"
                    image={this.state.image}
                    onPress={() => this.ActionSheet.show()}
                ></AccountPicture>
                <View>
                    <View style={styles.header}>
                        <Text style={styles.text}>My Account Information</Text>
                    </View>
                    <AccountInfoNav
                        icon={<IconEmailSvg />}
                        title="Email"
                        message="906380655@qq.com"
                        onPress={() => navigate('UpdateAccountInfo', {mode: Mode.email})}
                    ></AccountInfoNav>
                    <AccountInfoNav
                        icon={<IconPaymentMethodSvg />}
                        title="Default Payment"
                        message="MasterCard"
                        onPress={() => navigate('UpdatePaymentMethods')}
                    ></AccountInfoNav>
                    <AccountInfoNav
                        icon={<IconPhoneSvg />}
                        title="Tel"
                        message="222 1111 1111"
                        onPress={() => navigate('UpdateAccountInfo', {mode: Mode.phone})}
                    ></AccountInfoNav>
                    <AccountInfoNav
                        icon={<IconWaypointSvg />}
                        title="Address"
                        message="No.1048 Harbin xiang District songlei 3 floor"
                        onPress={() => navigate('AddressList')}
                    ></AccountInfoNav>
                </View>
                <ActionSheet
                    ref = { o => this.ActionSheet = o }
                    tintColor = '#666666'
                    options = {actionSheetButtons}
                    cancelButtonIndex = {2}
                    destructiveButtonIndex = {3}
                    onPress={this._handleActionSheetPress.bind(this)}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: 5,
        borderRadius: 4,
        backgroundColor: 'white',
        height: '100%'
    },
    header: {
        borderBottomWidth: 0.5,
        borderColor: '#efefef',
        justifyContent: 'center',
        height: 52,
        paddingLeft: 10
    },
    text: {
        fontSize: 18,
        color: '#666666'
    }
})
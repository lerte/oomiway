import * as React from 'react'
import { View, StyleSheet, Text, DeviceEventEmitter } from 'react-native'

import TextStyles from '../common/TextStyles'
import FooterButton from '../common/UIFooterButton'
import UITextInput from '../common/UITextInput'
import { BasicHeader } from '../common/Settings'

import { NavigationAction, NavigationScreenProp } from 'react-navigation'
import { addPaymentMethod, editPaymentMethod } from "../../redux/actions/index";
import { connect } from 'react-redux'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
    addPaymentMethod: any
    editPaymentMethod: any
    paymentMethods: object
}

interface State {
    editRow: number
    title: string
    cardNum: string
    cvv: string
    name: string
    expiry: string
    type: string
    incorrectFields: object
}

function mapStateToProps(state) {return {paymentMethods: state.paymentmethods.paymentMethods}}
function mapDispatchToProps(dispatch) {
    return {
        addPaymentMethod: (paymentMethod) => {
            dispatch(addPaymentMethod(paymentMethod))
        },
        editPaymentMethod: (paymentMethod, index) => {
            dispatch(editPaymentMethod(paymentMethod, index))
        }
    }
}
class AddCreditCardViewController extends React.Component<Props, State> {
    constructor(props) {
        super(props)

        const editing = this.props.navigation.state.params;
        const row = editing ? editing.edit : null;
        const rowToEdit = this.props.paymentMethods[row];

        this.state = {
            editRow: row ? row : null,
            cardNum: row ? rowToEdit.cardNum : "",
            cvv: row ? rowToEdit.cvv : "",
            name: row ? rowToEdit.name : "",
            title: row ? rowToEdit.title : "",
            expiry: row ? rowToEdit.expiry : "",
            type: row ? rowToEdit.type : "",
            incorrectFields: {cardNum: false, cvv: false, name: false, title: false, expiry: false},
        }
    }

    static navigationOptions = ({ navigation }) => {
        const paramsExist = navigation.state.params
        const edit = paramsExist ? paramsExist.edit : null
        const title = edit ? 'Edit Payment Method' : 'Add Payment Method'
        return BasicHeader(navigation, title)
    }

    _handleTitleChange(text) {
        this.setState({title: text})
    }

    _handleCardNumChange(text) {
        const cardNum = text.replace(/ /g, '')
        this.setState({cardNum: cardNum})
    }

    _formatCardNum(num) {
        let formatted = []
        for (let i = 0; i < num.length; i += 4) {
            formatted.push(num.substr(i, 4))
        }
        return formatted.join(' ')
    }


    _handleCVVChange(text) {
        this.setState({cvv: text})
    }

    _handleNameChange(text) {
        this.setState({name: text})
    }

    _handleExpiryChange(text) {
        let expiry = ""
        const monthYear = text.split('/')
        const prevExpiry = this.state.expiry
        const deleting = text.length < prevExpiry.length
        const month = monthYear[0]
        if (month > 1 && month.length == 1) { // add month to expiry
            expiry += this._pad(month)
        } else {
            expiry += month
        }
        // add year to expiry
        if (monthYear[1]) {
            expiry += monthYear[1]
        }
        // add slash
        if (!((deleting && expiry.length == 2) || expiry.length < 2)) {
            expiry = expiry.substr(0, 2) + '/' + expiry.substr(2)
        }
        // delete slash
        if (expiry.indexOf('/') == expiry.length - 1) {
            if (deleting) {
                expiry = expiry.substr(0, expiry.length - 1)
            }
        }
        // delete entire month if padded
        if (prevExpiry.length == 2 && deleting && prevExpiry.charAt(0) == "0") {
            expiry = ""
        }
        return this.setState({expiry: expiry})
    }

    _pad(n) { return "0" + n }

    _handleConfirmation() {
        const { goBack } = this.props.navigation
        const invalids: Array<string> = this._getInvalidInfo()
        if (invalids.length == 0) {
            const { title, cardNum, name, cvv, expiry, type } = this.state
            let paymentMethod = {title, cardNum, name, cvv, expiry, type}

            if (!this.state.editRow) {
                this.props.addPaymentMethod(paymentMethod)
            } else {
                this.props.editPaymentMethod(paymentMethod, this.state.editRow)
            }
            goBack()
            DeviceEventEmitter.emit('updatedPaymentMethods')
        } else {
            const incorrectFields = this.state.incorrectFields
            for (let field in incorrectFields) {
                if (invalids.indexOf(field) > -1) {
                    incorrectFields[field] = true
                } else {
                    incorrectFields[field] = false
                }
            }
            this.forceUpdate()
        }
    }

    _getInvalidInfo() {
        const { cardNum, cvv, expiry, name, title } = this.state
        let invalids = []

        if (cardNum.length !== 16) {invalids.push('cardNum')}
        if (cvv.length !== 3) {invalids.push('cvv')}
        if (name.length == 0) {invalids.push('name')}
        if (title.length == 0) {invalids.push('title')}

        const month = parseInt(expiry.substr(0, 2))
        const year = parseInt(expiry.substr(3, expiry.length))
        const currentYear = parseInt((new Date).getFullYear().toString().substr(2))
        if (expiry.length !== 5 || month > 12 || (year - currentYear > 4) || year < currentYear) {
            invalids.push('expiry')
        }

        return invalids
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.addCardContainer}>
                    <Text style={[TextStyles.regText, styles.headers]}>Add a credit card</Text>
                    <UITextInput
                        placeholder="Payment Method Name"
                        inputFieldStyle={styles.inputField}
                        containerStyle={[
                            styles.titleFieldLayout,
                            this.state.incorrectFields['title'] ? styles.incorrect : null
                        ]}
                        onChangeText={(text) => this._handleTitleChange(text)}
                        value={this.state.title}
                        autoFocus={!this.state.editRow}
                        autoCorrect={false}
                    ></UITextInput>
                    <Text style={[TextStyles.regText, styles.headers]}>Card Information</Text>
                    <View style={styles.row}>
                        <UITextInput
                            placeholder="Credit Card Number"
                            inputFieldStyle={styles.inputField}
                            containerStyle={[
                                styles.inputLayout,
                                styles.longInput,
                                this.state.incorrectFields['cardNum'] ? styles.incorrect : null
                            ]}
                            keyboardType={'number-pad'}
                            maxLength={19}
                            value={this._formatCardNum(this.state.cardNum)}
                            onChangeText={(text) => this._handleCardNumChange(text)}
                        ></UITextInput>
                        <UITextInput
                            placeholder="CVV"
                            inputFieldStyle={styles.inputField}
                            containerStyle={[
                                styles.inputLayout,
                                styles.shortInput,
                                this.state.incorrectFields['cvv'] ? styles.incorrect : null
                            ]}
                            maxLength={3}
                            keyboardType={'number-pad'}
                            onChangeText={(text) => this._handleCVVChange(text)}
                            value={this.state.cvv}
                        >
                        </UITextInput>
                    </View>
                    <View style={styles.row}>
                        <UITextInput
                            placeholder="Name on Card"
                            inputFieldStyle={styles.inputField}
                            containerStyle={[
                                styles.inputLayout,
                                styles.longInput,
                                this.state.incorrectFields['name'] ? styles.incorrect : null
                            ]}
                            onChangeText={(text) => this._handleNameChange(text)}
                            value={this.state.name}
                            autoCorrect={false}
                        >
                        </UITextInput>
                        <UITextInput
                            placeholder="MM/YY"
                            inputFieldStyle={styles.inputField}
                            containerStyle={[
                                styles.inputLayout,
                                styles.shortInput,
                                this.state.incorrectFields['expiry'] ? styles.incorrect : null
                            ]}
                            keyboardType={'number-pad'}
                            onChangeText={(text) => this._handleExpiryChange(text)}
                            value={this.state.expiry}
                            maxLength={5}
                        ></UITextInput>
                    </View>
                </View>
                <FooterButton
                    title="Confirm"
                    onPress={() => this._handleConfirmation()}
                ></FooterButton>
            </View>
        )
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddCreditCardViewController)

const styles = StyleSheet.create({
    container: {
        height: '100%',
        justifyContent: 'space-between'
    },
    addCardContainer: {
        backgroundColor: 'white',
        borderRadius: 4,
        marginHorizontal: 5,
        paddingTop: 15,
        paddingBottom: 30,
        paddingHorizontal: 10
    },
    titleFieldLayout: {
        marginBottom: 30,
        marginTop: 10
    },
    inputLayout: {
        marginTop: 15
    },
    incorrect: {
        borderBottomColor: 'red'
    },
    inputField: {
        height: 30,
        fontSize: 15,
        color: '#666666',
        flex: 1
    },
    headers: {
        marginLeft: 5,
    },
    row: {
        flexDirection: 'row'
    },
    longInput: {
        flex: 1,
        marginRight: 20,
        marginLeft: 10
    },
    shortInput: {
        width: 70,
        marginRight: 10
    },
})
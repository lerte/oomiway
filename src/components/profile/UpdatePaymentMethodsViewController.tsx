import * as React from 'react'
import { View } from 'react-native'

import PaymentMethodsListview from '../main/PaymentMethods/PaymentMethodListView'
import { BasicHeader } from '../common/Settings'
import FooterButton from '../common/UIFooterButton'

import { NavigationAction, NavigationScreenProp } from 'react-navigation'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
}

interface State {
    selectedRow: number
}

export default class UpdatePaymentMethodsViewController extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        this.state = {selectedRow: null}
    }

    static navigationOptions = ( {navigation} ) => BasicHeader(navigation, 'Payment Methods')

    _getSelectedRow(row) {
        this.setState({selectedRow: row})
    }

    render() {
        const { navigate, goBack } = this.props.navigation
        return (
            <View style={{height: '100%'}}>
                <PaymentMethodsListview
                    addButtonPressed={() => navigate('AddCreditCard')}
                    onEditBtnPress={() => navigate('AddCreditCard', {edit: this.state.selectedRow})}
                    onRowPress={this._getSelectedRow.bind(this)}
                >
                </PaymentMethodsListview>
                <FooterButton
                    title="Confirm"
                    onPress={() => goBack()}
                ></FooterButton>
            </View>
        )
    }
}
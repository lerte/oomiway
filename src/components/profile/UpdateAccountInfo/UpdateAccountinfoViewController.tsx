import * as React from 'react'
import {
    View, StyleSheet, Text, Animated, Dimensions, TouchableOpacity, Keyboard
} from 'react-native'

import TextStyles from '../../common/TextStyles'
import { BasicHeader } from '../../common/Settings'
import { IconUpdateEmailSvg, IconUpdatePhoneSvg } from '../../common/IconsSvg'
import SendInputField from './SendInputField'
import VerificationInput from './VerificationInput'
import { Mode } from '../AccountProfile/AccountProfileViewController'

import { NavigationAction, NavigationScreenProp } from'react-navigation'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
}

interface State {
    sendInputValue: string
    verificationInputValue: string
}

const correctCode = "123456"
const deviceWidth = Dimensions.get('window').width - 10
export default class UpdatePhoneNumberViewController extends React.Component<Props, State> {
    static navigationOptions = ( {navigation} ) => BasicHeader(
        navigation, 
        'Update '+UpdatePhoneNumberViewController._getTitle(navigation.state.params.mode).long
    )
    constructor(props) {
        super(props)
        this.state = {sendInputValue: "", verificationInputValue: ""}
    }

    verifyAnimVal = new Animated.Value(0)
    wrongCodeAnimVal = new Animated.Value(0)
    private verificationInput
    private sendInput

    _checkVerificationCode(code) {
        if (code == correctCode) {
            this._correctVerificationInput()
        } else {
            this._wrongVerificationInput()
        }
    }

    _correctVerificationInput() {
        const { goBack } = this.props.navigation
        console.log('Account information updated')
        goBack()
        goBack()
    }

    _wrongVerificationInput() {
        this.verificationInput.clear()
        this.setState({verificationInputValue: ""})
        this.wrongCodeAnimVal.setValue(0)
        Animated.timing(this.wrongCodeAnimVal,
            {
                toValue: 1,
                duration: 300
            }
        ).start()
    }

    _verifyEmailFormat(s) {
        const m = s.match(/[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@?[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])*\.(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9]))+/)
        return m ? true : false
    }

    _verifyPhoneFormat(s) {
        const m = s.match(/[0-9]{7,}/)
        return m ? true : false
    }

    _goToVerifyScreen() {
        this.verificationInput.focus()
        Animated.timing(this.verifyAnimVal,
            {
                toValue: 1,
                duration: 400
            }
        ).start()
    }

    _goToInputScreen() {
        this.sendInput.focus()
        Animated.timing(this.verifyAnimVal,
            {
                toValue: 0,
                duration: 400
            }
        ).start()
    }

    static _toTitleCase(s) {
        return s.replace(/\w\S*/g, (t) => {
            return t.charAt(0).toUpperCase() + t.substr(1)
        })
    }

    static _getTitle(mode) {
        let long, sent
        if (mode == Mode.email) {
            long = UpdatePhoneNumberViewController._toTitleCase("email address")
            sent = "Email"
        } else if (mode == Mode.phone) {
            long = UpdatePhoneNumberViewController._toTitleCase("phone number")
            sent = "SMS"
        }
        return{
            long,
            sent
        }
    }
    static _getIcon(mode):React.Component {
        let icon
        if (mode == Mode.email) {
            icon = <IconUpdateEmailSvg />  
        } else if (mode == Mode.phone) {
            icon = <IconUpdatePhoneSvg />  
        }
        return icon
    }
    render() {
        const { state } = this.props.navigation
        const { mode } = state.params
        const { long, sent } = UpdatePhoneNumberViewController._getTitle(mode)
        
        const verifyAnimVal = this.verifyAnimVal.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 0 - deviceWidth]
        })

        const wrongCodeAnimVal = this.wrongCodeAnimVal.interpolate({
            inputRange: [0, 0.17, 0.34, 0.5, 0.67, 0.84, 1],
            outputRange: [0, -10, 0, 10, 0, -10, 0]
        })

        return (
            <View
                style={styles.container}
            >
                <View style={styles.iconContainer}>
                    <View style={styles.icon}>
                        {UpdatePhoneNumberViewController._getIcon(mode)}     
                    </View>
                </View>
                <Animated.View
                    style={[
                        {
                            transform: [{translateX: verifyAnimVal}]
                        },
                        styles.animCont
                    ]}
                >
                    <View style={styles.sendContainer}>
                        <Text style={[TextStyles.smallDark, styles.header]}>Update {long}</Text>
                        <View  style={styles.desc}>
                            <Text style={[TextStyles.verySmallDark, {textAlign: 'center'}]}>
                                Please enter your {long}. {"\n"}
                                We will send you a verification code
                            </Text>
                        </View>
                        <SendInputField
                            onPress={() => {
                                Keyboard.dismiss()
                                const val = this.state.sendInputValue
                                if ((!this._verifyEmailFormat(val) && mode == Mode.email)
                                    || (!this._verifyPhoneFormat(val) && mode == Mode.phone)) {
                                    console.log(`Invalid ${long}`)
                                } else {
                                    this._goToVerifyScreen()
                                }
                            }}
                            inputRef={(input) => this.sendInput = input}
                            mode={mode}
                            onChangeText={(text) => this.setState({sendInputValue: text})}
                        ></SendInputField>
                    </View>
                    <View style={styles.verifyContainer}>
                        <Text style={[TextStyles.smallDark, styles.header]}>The verification code has been sent</Text>
                        <View style={styles.desc}>
                            <Text style={TextStyles.verySmallDark}>
                                {sent} has been sent to {this.state.sendInputValue}
                            </Text>
                            <TouchableOpacity
                                onPress={() => {
                                    Keyboard.dismiss()
                                    this._goToInputScreen()
                                }}
                                hitSlop={{
                                    top: 20, bottom: 20, right: 20, left: 20
                                }}
                            >
                                <Text style={TextStyles.smallOrange}>Change</Text>
                            </TouchableOpacity>
                        </View>
                        <Animated.View
                            style={[
                                styles.verify,
                                {transform: [{translateX: wrongCodeAnimVal}]}
                            ]}
                        >
                            <VerificationInput
                                inputRef={(input) => this.verificationInput = input}
                                onChangeText={(text) => {
                                    this.setState({verificationInputValue: text})
                                    if (text.length == 6) {
                                        this._checkVerificationCode(text)
                                    }
                                }}
                                value={this.state.verificationInputValue}
                            ></VerificationInput>
                        </Animated.View>
                    </View>
                </Animated.View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        marginHorizontal: 5,
        borderRadius: 4,
        backgroundColor: 'white',
        overflow: 'hidden',
        paddingHorizontal: 25
    },
    iconContainer: {
        alignItems: 'center'
    },
    icon: {
        height: 70,
        width: 70,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#666666',
        borderWidth: 2,
        borderRadius: 70,
        marginTop: 80
    },
    animCont: {
        flexDirection: 'row',
    },
    sendContainer: {
        width: '100%',
        marginRight: 50
    },
    verifyContainer: {
        width: '100%',
        alignItems: 'center'
    },
    header: {
        marginVertical: 20,
        textAlign: 'center',
    },
    desc: {
        height: 40,
        alignItems: 'center'
    },
    verify: {
        width: '100%'
    }
})
import * as React from 'react'
import { View, TextInput, StyleSheet, Text } from 'react-native'

interface Props {
    focus?: boolean
    inputRef?: any
    selectionState?: any
    onChangeText?: any
    value: string
}

export default class VerificationInput extends React.Component<Props, undefined> {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.inputRow}>
                    <View style={[styles.numInput, 0 == this.props.value.length ? styles.activeInput : null]}>
                        <Text style={styles.num}>{this.props.value.charAt(0)}</Text>
                    </View>
                    <View style={[styles.numInput, 1 == this.props.value.length ? styles.activeInput : null]}>
                        <Text style={styles.num}>{this.props.value.charAt(1)}</Text>
                    </View>
                    <View style={[styles.numInput, 2 == this.props.value.length ? styles.activeInput : null]}>
                        <Text style={styles.num}>{this.props.value.charAt(2)}</Text>
                    </View>
                    <View style={[styles.numInput, 3 == this.props.value.length ? styles.activeInput : null]}>
                        <Text style={styles.num}>{this.props.value.charAt(3)}</Text>
                    </View>
                    <View style={[styles.numInput, 4 == this.props.value.length ? styles.activeInput : null]}>
                        <Text style={styles.num}>{this.props.value.charAt(4)}</Text>
                    </View>
                    <View style={[styles.numInput, 5 == this.props.value.length ? styles.activeInput : null]}>
                        <Text style={styles.num}>{this.props.value.charAt(5)}</Text>
                    </View>
                </View>
                <TextInput
                    style={styles.input}
                    autoFocus={this.props.focus}
                    ref={(input) => this.props.inputRef(input)}
                    selectionState={this.props.selectionState}
                    keyboardType={'number-pad'}
                    maxLength={6}
                    onChangeText={this.props.onChangeText}
                ></TextInput>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 40,
        marginTop: 30,
        justifyContent: 'flex-end',
    },
    input: {
        height: 0,
        width: 0,
        overflow: 'hidden'
    },
    inputRow: {
        paddingRight: 10,
        flexDirection: 'row',
    },
    numInput: {
        borderBottomWidth: 1,
        borderColor: '#979797',
        flex: 1,
        marginLeft: 10,
        alignItems: 'center',
        paddingBottom: 5,
    },
    activeInput: {
        borderBottomColor: '#f26f21',
        borderBottomWidth: 3
    },
    num: {
        fontSize: 18,
        color: '#666666'
    }
})
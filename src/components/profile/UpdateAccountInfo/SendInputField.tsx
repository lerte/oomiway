import * as React from 'react'
import { TouchableOpacity, StyleSheet, View, Text } from 'react-native'

import UITextInput from '../../common/UITextInput'

interface Props {
    onPress: any
    mode: string
    onChangeText: any
    inputRef?: any
}

export default class SendInputField extends React.Component<Props, undefined> {
    render() {
        return (
            <View style={styles.inputContainer}>
                <UITextInput
                    containerStyle={styles.inputBody}
                    inputFieldStyle={styles.inputField}
                    autoFocus={true}
                    keyboardType={this.props.mode == "phone" ? 'number-pad' : 'default'}
                    onChangeText={this.props.onChangeText}
                    inputRef={this.props.inputRef}
                ></UITextInput>
                <TouchableOpacity
                    style={styles.sendButton}
                    onPress={this.props.onPress}
                >
                    <Text style={styles.sendButtonText}>Send</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    inputContainer: {
        marginTop: 35,
        flexDirection: 'row',
        width: '100%'
    },
    inputBody: {
        flex: 1,
        marginRight: 10,
    },
    inputField: {
        marginBottom: 5,
        flex: 1
    },
    sendButton: {
        height: 35,
        width: 70,
        backgroundColor: '#f26f21',
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    sendButtonText: {
        color: 'white',
        fontSize: 18
    },
})
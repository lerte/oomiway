import * as React from 'react'
import { View, StyleSheet, DeviceEventEmitter, Text } from 'react-native'

import FooterButton from '../common/UIFooterButton'
import UITextInput from '../common/UITextInput'
import { BasicHeader } from '../common/Settings'
import TextStyles from '../common/TextStyles'

import { NavigationAction, NavigationScreenProp } from 'react-navigation'
import { connect } from 'react-redux'
import { addAddress, editAddress } from '../../redux/actions/index'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
    addAddress: any
    editAddress: any
    addresses: object
}

interface State {
    editRow: number
    address: string
    city: string
    state: string
    zip: string
    country: string
    incorrectFields: object
}

function mapStateToProps(state) {return {addresses: state.addresses.addresses}}
function mapDispatchToProps(dispatch) {
    return {
        addAddress: (address) => {
            dispatch(addAddress(address))
        },
        editAddress: (address, index) => {
            dispatch(editAddress(address, index))
        }
    }
}
class AddAddressViewController extends React.Component<Props, State> {
    static navigationOptions = ({ navigation }) => BasicHeader(
        navigation, navigation.state.params? 'Edit Address' : 'Address a New Address'
    )
    constructor(props) {
        super(props)

        const editExists = this.props.navigation.state.params
        const row = editExists ? editExists.edit : null
        const rowToEdit = this.props.addresses[row]

        this.state = {
            editRow: editExists ? row : null,
            address: editExists ? rowToEdit.address : "",
            city: editExists ? rowToEdit.city : "",
            state: editExists ? rowToEdit.state : "",
            zip: editExists ? rowToEdit.zip : "",
            country: editExists ? rowToEdit.country : "",
            incorrectFields: {address: false, city: false, state: false, zip: false, country: false},
        }
    }

    _handleAddressChange(text) {
        this.setState({address: text})
    }

    _handleCityChange(text) {
        this.setState({city: text})
    }

    _handleStateChange(text) {
        this.setState({state: text})
    }

    _handleZipChange(text) {
        this.setState({zip: text})
    }

    _handleCountryChange(text) {
        this.setState({country: text})
    }

    _handleConfirmation() {
        const { goBack } = this.props.navigation
        const invalids: Array<string> = this._getInvalidInfo()
        if (invalids.length == 0) {
            let { address, city, state, zip, country } = this.state
            state = state.toUpperCase()
            const fullAddress = {address, city, state, zip, country}
            if (!this.state.editRow) {
                this.props.addAddress(fullAddress)
            } else {
                this.props.editAddress(fullAddress, this.state.editRow)
            }
            goBack()
            DeviceEventEmitter.emit('addedAddress')
        } else {
            const incorrectFields = this.state.incorrectFields
            for (let field in incorrectFields) {
                if (invalids.indexOf(field) > -1) {
                    incorrectFields[field] = true
                } else {
                    incorrectFields[field] = false
                }
            }
            this.forceUpdate()
        }
    }

    _getInvalidInfo() {
        const { address, city, state, zip, country } = this.state
        let invalids = []

        if (address.length == 0) {invalids.push('address')}
        if (city.length == 0) {invalids.push('city')}
        if (state.length !== 2) {invalids.push('state')}
        if (zip.length != 5) {invalids.push('zip')}
        if (country.length == 0) {invalids.push('country')}

        return invalids
    }

    render() {

        return (
            <View style={styles.container}>
                <View style={styles.addAddressContainer}>
                    <View style={styles.header}>
                        <Text style={TextStyles.orangeReg}>
                            {this.state.editRow ? 'Edit Address' : 'Add a New Address'}
                        </Text>
                    </View>
                    <View style={styles.row}>
                        <UITextInput
                            inputFieldStyle={styles.inputField}
                            containerStyle={[
                                styles.inputLayout,
                                this.state.incorrectFields['address'] ? styles.incorrect : null
                            ]}
                            prefixIcon={<Text style={TextStyles.smallText}>Address </Text>}
                            onChangeText={(text) => this._handleAddressChange(text)}
                            value={this.state.address}
                            autoFocus={!this.state.editRow}
                            autoCorrect={false}
                        ></UITextInput>
                    </View>
                    <View style={styles.row}>
                        <UITextInput
                            inputFieldStyle={styles.inputField}
                            containerStyle={[
                                styles.inputLayout,
                                styles.leftInput,
                                this.state.incorrectFields['city'] ? styles.incorrect : null
                            ]}
                            prefixIcon={<Text style={TextStyles.smallText}>City </Text>}
                            value={this.state.city}
                            onChangeText={(text) => this._handleCityChange(text)}
                        ></UITextInput>
                        <UITextInput
                            inputFieldStyle={styles.inputField}
                            containerStyle={[
                                styles.inputLayout,
                                this.state.incorrectFields['state'] ? styles.incorrect : null
                            ]}
                            prefixIcon={<Text style={TextStyles.smallText}>State </Text>}
                            onChangeText={(text) => this._handleStateChange(text)}
                            value={this.state.state}
                            maxLength={2}
                        >
                        </UITextInput>
                    </View>
                    <View style={styles.row}>
                        <UITextInput
                            inputFieldStyle={styles.inputField}
                            containerStyle={[
                                styles.inputLayout,
                                styles.leftInput,
                                this.state.incorrectFields['zip'] ? styles.incorrect : null
                            ]}
                            prefixIcon={<Text style={TextStyles.smallText}>Zip Code </Text>}
                            onChangeText={(text) => this._handleZipChange(text)}
                            value={this.state.zip}
                            keyboardType={'number-pad'}
                            maxLength={5}
                        >
                        </UITextInput>
                        <UITextInput
                            inputFieldStyle={styles.inputField}
                            containerStyle={[
                                styles.inputLayout,
                                this.state.incorrectFields['country'] ? styles.incorrect : null
                            ]}
                            prefixIcon={<Text style={TextStyles.smallText}>Country </Text>}
                            onChangeText={(text) => this._handleCountryChange(text)}
                            value={this.state.country}
                        ></UITextInput>
                    </View>
                </View>
                <FooterButton
                    title="Confirm"
                    onPress={() => this._handleConfirmation()}
                >
                </FooterButton>
            </View>
        )
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddAddressViewController)

const styles = StyleSheet.create({
    container: {
        height: '100%',
        justifyContent: 'space-between'
    },
    addAddressContainer: {
        backgroundColor: 'white',
        marginHorizontal: 5,
        borderRadius: 4,
        paddingHorizontal: 10,
        paddingBottom: 30,
        paddingTop: 15
    },
    header: {
        marginLeft: 5,
        marginBottom: 20
    },
    headerText: {
        fontSize: 18,
        color: '#666666'
    },
    incorrect: {
        borderBottomColor: 'red'
    },
    inputLayout: {
        marginTop: 15,
        flex: 1,
        justifyContent: 'center'
    },
    inputField: {
        flex: 1,
        height: 30,
        color: '#666666',
        fontSize: 18
    },
    row: {
        flexDirection: 'row',
    },
    leftInput: {
        marginRight: 15
    }
})
import * as React from 'react'
import { View } from 'react-native'

import OrderInfoListView from '../../main/OrderConfirm/OrderInfoListView'
import PaymentReceivedHeader from './PaymentReceivedHeader'
import FooterButton from '../../common/UIFooterButton'

import { StackActions, NavigationActions, NavigationScreenProp, NavigationAction } from 'react-navigation'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
}

export default class PaymentReceivedViewController extends React.Component<Props, undefined> {
    static navigationOptions = {
        header: <View style={{backgroundColor: '#EFEFEF', height: 32}}></View>
    }

    _goBackToHomeScreen() {
        return (
            this.props.navigation.dispatch(StackActions.reset(
                {
                    index: 0,
                    actions: [
                        NavigationActions.navigate({ routeName: 'Home' })
                    ]
                }
            ))
        )
    }

    render() {
        return (
            <View style={{height: '100%'}}>
                <PaymentReceivedHeader></PaymentReceivedHeader>
                <View style={{borderRadius: 4, marginHorizontal: 5, backgroundColor: 'white', flex: 1}}>
                    <OrderInfoListView
                        header="Sale List"
                        purchaseCompleteView={true}
                    ></OrderInfoListView>
                </View>
                <FooterButton
                    title="Done"
                    onPress={() => this._goBackToHomeScreen()}
                ></FooterButton>
            </View>
        )
    }
}
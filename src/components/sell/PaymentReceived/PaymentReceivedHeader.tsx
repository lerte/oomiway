import * as React from 'react'
import { View, StyleSheet, Text } from 'react-native'

import TextStyles from '../../common/TextStyles'
import BigMoneyLayout from '../../common/BigMoneyLayout'
import { IconWalletCoinsSvg } from '../../common/IconsSvg'

export default class PaymentReceivedHeader extends React.Component<any, undefined> {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.icon}>
                    <IconWalletCoinsSvg />
                </View>
                <Text style={TextStyles.regText}>Payment Received</Text>
                <BigMoneyLayout
                    amount="709.50"
                    color="#f26f21"
                ></BigMoneyLayout>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 280,
        borderRadius: 4,
        backgroundColor: 'white',
        margin: 5,
        marginTop: 0,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingVertical: 25
    },
    icon: {
        height: 140,
        width: 100
    }
})
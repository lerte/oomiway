import * as React from 'react'
import { View, Text, StyleSheet, Button } from 'react-native'

import TextStyles from '../common/TextStyles'
import BigMoneyLayout from '../common/BigMoneyLayout'
import {BasicHeader} from '../common/Settings'
import QRCode from 'react-native-qrcode-svg'

import { NavigationAction, NavigationScreenProp } from 'react-navigation'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
}

export default class ReceiveMoneyViewController extends React.Component<Props, undefined> {
    static navigationOptions = ( {navigation} ) => BasicHeader(navigation, 'Receive Money')
    render() {
        const { navigate } = this.props.navigation
        let logoFromFile = require('../../../icon.png');
        return (
            <View style={styles.container}>
                <Text style={TextStyles.regText}>Order QR Code</Text>
                <View style={styles.QRCode}>
                    <QRCode value="SCSCSCSVDVFNHMNGNFNGHNGNNBDBDDBFBFB" logo={logoFromFile} color="#F26F21" size={220} />
                </View>
                <Text style={[TextStyles.smallText, styles.instr]}>Have your customer scan this QR code to link up to your order.</Text>
                <BigMoneyLayout amount="709.50" color="#666666"></BigMoneyLayout>
                <Button
                    title="Next"
                    onPress={() => navigate('PaymentReceived')}
                ></Button>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        marginLeft: 5,
        marginRight: 5,
        borderRadius: 4,
        backgroundColor: 'white',
        alignItems: 'center',
        paddingTop: 40,
    },
    QRCode: {
        marginVertical: 35
    },
    instr: {
        width: '80%',
        textAlign: 'center',
        marginBottom: 30
    }
})
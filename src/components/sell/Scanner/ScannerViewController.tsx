import * as React from 'react'
import { View, TouchableOpacity, StyleSheet, Animated, Dimensions } from 'react-native'

import CodeScanner from './CodeScanner'
import ScannedItemList from './ScannedItemList'
import { IconCartSvg } from '../../common/IconsSvg'
import { BasicHeader } from '../../common/Settings'
import FooterMoneyTotal from '../../common/FooterMoneyTotal'
import FooterButton from '../../common/UIFooterButton'

import { NavigationAction, NavigationScreenProp } from 'react-navigation'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
}

interface State {
    expandedCart: boolean
}

const cameraHeight = Dimensions.get('window').width
const maxCartHeight = Dimensions.get('window').height - 65
const animDuration = 600
export default class ScannerViewController extends React.Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {expandedCart: false}
    }

    static navigationOptions = ({navigation}) => BasicHeader(navigation, 'Scan')

    animVal = new Animated.Value(0)
    
    _moveScreenUp() {
        this.setState({expandedCart: true})
        Animated.timing(this.animVal,
            {
                toValue: 1,
                duration: animDuration
            }
        ).start()
    }

    _moveScreenDown() {
        Animated.timing(this.animVal,
            {
                toValue: 0,
                duration: animDuration
            }
        ).start()
        setTimeout(() => {
            this.setState({expandedCart: false})
        }, animDuration)
    }

    _renderFooter(amount: string, onPress: any) {
        return (
            <View>
                <FooterMoneyTotal
                    amount={amount}
                ></FooterMoneyTotal>
                <FooterButton
                    title="Done"
                    onPress={onPress}
                ></FooterButton>
            </View>
        )
    }

    render() {
        const moveUp = this.animVal.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 0 - cameraHeight]
        })

        const { navigate } = this.props.navigation
        return (
            <View style={styles.container}>
                <Animated.View
                    style={{
                        transform: [{translateY: moveUp}]
                    }}
                >
                    <CodeScanner></CodeScanner>
                    <View
                        style={
                            this.state.expandedCart ?
                            {height: maxCartHeight - 109} : // 109 for the footer
                            {height: maxCartHeight - cameraHeight}
                        }
                    >
                        <View style={styles.btnContainer}>
                            <TouchableOpacity
                                style={styles.cartButton}
                                onPress={this.state.expandedCart ?
                                    this._moveScreenDown.bind(this) :
                                    this._moveScreenUp.bind(this)}
                            >
                                <IconCartSvg></IconCartSvg>
                            </TouchableOpacity>
                        </View>
                        <ScannedItemList></ScannedItemList>
                    </View>
                    {this._renderFooter('709.50', () => navigate('ReceiveMoney'))}
                </Animated.View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%'
    },
    btnContainer: {
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        width: '100%',
        marginHorizontal: 5,
        alignItems: 'center'
    },
    cartButton: {
        height: 45,
        width: 45,
        borderRadius: 45,
        backgroundColor: '#f26f21',
        marginTop: -22.5,
        alignItems: 'center',
        justifyContent: 'center'
    },
})
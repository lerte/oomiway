import * as React from 'react'
import { Animated, Easing, View, StyleSheet, Dimensions, Vibration } from 'react-native'
import { RNCamera } from 'react-native-camera'
import { IconScanningLineSvg } from '../../common/IconsSvg'

interface bounds {
    origin: {
        x: number
        y: number
    }
    size: {
        width: number
        height: number
    }
}
interface State {
    scanning: boolean
    codeBounds: bounds
}

export default class CodeScanner extends React.Component<any, State> {
    private animVal = new Animated.Value(0)
    constructor(private camera: RNCamera, props) {
        super(props)
        this.state = { scanning: true, codeBounds: {origin:{x:0,y:0},size:{width:0,height:0}} }
    }

    _onBarCodeRead(e) {
        if (this.state.scanning == false) { return null }
        this.camera.takePictureAsync()
        this.setState({scanning: false, codeBounds: e.bounds})
        Vibration.vibrate(0, false)
        console.log(e.data)
        setTimeout(() => {
            this.setState({scanning: true, codeBounds: {origin:{x:0,y:0},size:{width:0,height:0}} })
        }, 3000)
    }

    _onScanning() {
        const moving = this.animVal.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 0 + scannerSize]
        })
        return (
            <Animated.View
                style={{
                    transform: [{translateY: moving}],
                    left: borderSize,
                    top: borderSize,
                    width: scannerSize,
                    position: 'absolute',
                    zIndex: 20
                }}
            >
                <IconScanningLineSvg></IconScanningLineSvg>
            </Animated.View>
        )
    }

    _highlightBarcode(bounds) {
        let { x, y } = bounds.origin
        let { height, width } = bounds.size
        x = parseInt(x); y = parseInt(y); height = parseInt(height); width = parseInt(width)
        return (
            <View
                style={{
                    left: x,
                    bottom: y,
                    height: height,
                    width: width,
                    position: 'absolute',
                    borderWidth: 5,
                    zIndex: 20
                }}
            ></View>
        )
    }

    componentDidMount() {
        this._movingDown()
    }

    _movingDown() {
        Animated.timing(this.animVal,
            {
                toValue: 1,
                easing: Easing.ease,
                duration: 3000,
                useNativeDriver: true
            }
        ).start(() => this._movingUp())
    }

    _movingUp() {
        Animated.timing(this.animVal,
            {
                toValue: 0,
                easing: Easing.ease,
                duration: 3000,
                useNativeDriver: true
            }
        ).start(() => this._movingDown())
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.darkOverlay}></View>
                { this.state.scanning ? this._onScanning() : null }
                <RNCamera
                    ref={ ref => this.camera = ref }
                    style={styles.preview}
                    onBarCodeRead={this._onBarCodeRead.bind(this)}
                    type={RNCamera.Constants.Type.back}
                >
                    <View style={styles.rect}>
                        <View style={styles.borderBoxRow}>
                            <View style={[styles.borderBox, styles.topLeft]}></View>
                            <View style={[styles.borderBox, styles.topRight]}></View>
                        </View>
                        <View style={styles.borderBoxRow}>
                            <View style={[styles.borderBox, styles.bottomLeft]}></View>
                            <View style={[styles.borderBox, styles.bottomRight]}></View>
                        </View>
                    </View>
                </RNCamera>
            </View>
        )
    }
}

const screenWidth = Dimensions.get('window').width
const scannerSize = 250
const borderSize = (screenWidth - scannerSize) / 2
const borderBoxSize = scannerSize / 10
const styles = StyleSheet.create({
    container: {
        height: screenWidth,
        backgroundColor: 'black'
    },
    darkOverlay: {
        backgroundColor: 'transparent',
        borderWidth: borderSize,
        opacity: 0.6,
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 1
    },
    preview: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    rect: {
        height: scannerSize,
        width: scannerSize,
        backgroundColor: 'transparent',
        justifyContent: 'space-between',
        padding: 0
    },
    borderBoxRow: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    borderBox: {
        height: borderBoxSize,
        width: borderBoxSize,
        borderColor: '#f26f21'
    },
    topLeft: {
        borderTopWidth: 2,
        borderLeftWidth: 2
    },
    topRight: {
        borderTopWidth: 2,
        borderRightWidth: 2
    },
    bottomLeft: {
        borderBottomWidth: 2,
        borderLeftWidth: 2
    },
    bottomRight: {
        borderBottomWidth: 2,
        borderRightWidth: 2
    }
})
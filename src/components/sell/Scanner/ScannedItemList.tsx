import * as React from 'react'
import { ListView, View } from 'react-native'

import SingleScannedItem from './SingleScannedItem'

interface State {
    dataSource: any
}

let exampleData = [
    {
        name: 'Plug',
        price: '54.95',
        desc: "Get wireless control of don cheadle"
    },
    {
        name: 'Air',
        price: '199.90',
        desc: "Attach Oomi Air to the don cheadle"
    },
    { name: '3', price: '3', desc: '3' },
    { name: '4', price: '4', desc: '4' },
    { name: '5', price: '5', desc: '5' }
]

export default class CartListView extends React.Component<any, State> {
    constructor(props) {
        super(props)
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
        this.state = {dataSource: ds.cloneWithRows(exampleData)}
    }

    _renderRow(data) {
        return (
            <SingleScannedItem
                name={data.name}
                price={data.price}
                desc={data.desc}
            ></SingleScannedItem>
        )
    }

    render() {
        return(
            <ListView
                style={{marginHorizontal: 5,borderRadius: 4, backgroundColor: 'white'}}
                scrollEnabled={true}
                dataSource={this.state.dataSource}
                renderRow={this._renderRow}
                renderHeader={() => {
                    return (<View style={{height: 20}}></View>)
                }}
            ></ListView>
        )
    }
}
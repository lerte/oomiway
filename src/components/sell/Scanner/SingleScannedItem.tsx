import * as React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import TextStyles from '../../common/TextStyles'
//import PlusMinusButtonGroup from '../../common/CartButtonGroup'

interface Props {
    name: string,
    desc: string,
    price: number
}

export default class SingleCartItem extends React.Component<Props, undefined> {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.thumbnail}></View>
                <View style={styles.info}>
                    <View style={styles.desc}>
                        <View style={styles.flexRow}>
                            <Text style={TextStyles.regText}>{this.props.name}</Text>
                        </View>
                        <Text
                            style={[TextStyles.smallText, styles.smallDesc]}
                            numberOfLines={1}
                        >{this.props.desc}
                        </Text>
                    </View>
                    <View style={styles.flexRow}>
                        <Text style={TextStyles.orangeReg}>${(this.props.price * 1).toFixed(2)}</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 130,
        backgroundColor: 'white',
        flexDirection: 'row',
        paddingHorizontal: 8,
        paddingVertical: 22.5,
        borderBottomWidth: 0.5,
        borderColor: '#efefef',
        alignItems: 'center'
    },
    thumbnail: {
        height: 95,
        width: 95,
        backgroundColor: '#dddddd',
        borderRadius: 4,
        marginRight: 8
    },
    info: {
        width: '70%',
        justifyContent: 'space-between',
        height: 95
    },
    flexRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end'
    },
    desc: {
        height: 45,
        justifyContent: 'space-between'
    },
    smallDesc: {
        width: '75%'
    }
})

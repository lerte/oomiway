import * as React from 'react'
import { TouchableOpacity, StyleSheet } from 'react-native'

import { IconCircleAddButtonSvg } from './IconsSvg'

interface Props {
    onPress: any
}


export default class UIAddButton extends React.Component<Props, undefined> {
    render() {
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                style={styles.container}
            >
                <IconCircleAddButtonSvg></IconCircleAddButtonSvg>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 30,
        borderRadius: 50,
        backgroundColor: '#f26f21'
    },
})
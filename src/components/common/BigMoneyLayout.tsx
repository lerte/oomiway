import * as React from 'react'
import { View, Text, StyleSheet } from 'react-native'

interface Props {
    amount: string
    color: string
}

export default class BigMoneyLayout extends React.Component<Props, undefined> {
    render() {
        return (
            <View style={styles.amountContainer}>
                <Text style={[styles.currencyType, {color: this.props.color}]}>$</Text>
                <Text style={[styles.moneyAmount, {color: this.props.color}]}>{this.props.amount}</Text>
                <Text style={[styles.currencyType, {color: this.props.color}]}>USD</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    amountContainer: {
        flexDirection: 'row'
    },
    currencyType: {
        fontSize: 13,
        color: '#f26f21',
        marginTop: 8,
        marginHorizontal: 2
    },
    moneyAmount: {
        fontSize: 30,
        color: '#f26f21'
    }
})
import * as React from 'react'
import { View, TextInput, StyleSheet } from 'react-native'

interface Props {
    prefixIcon?: any
    suffixIcon?: any
    placeholder?: string
    containerStyle?: any
    inputFieldStyle?: any
    onChangeText?: any
    maxLength?: number
    value?: string
    secureTextEntry?: boolean
    autoFocus?: boolean
    keyboardType?: any
    inputRef?: any
    autoCorrect?: boolean
    onFocus?: any
    onBlur?: any
}

export default class UITextInput extends React.Component<Props, undefined> {
    render() {
        return (
            <View style={[styles.inputLayout, this.props.containerStyle]}>
                {this.props.prefixIcon}
                <TextInput
                    style={[styles.inputField, this.props.inputFieldStyle]}
                    placeholder={this.props.placeholder}
                    onChangeText={this.props.onChangeText}
                    maxLength={this.props.maxLength}
                    value={this.props.value}
                    selectionColor='#F26F21'
                    underlineColorAndroid='rgba(0,0,0,0)'
                    secureTextEntry={this.props.secureTextEntry}
                    autoFocus={this.props.autoFocus}
                    keyboardType={this.props.keyboardType}
                    autoCorrect={this.props.autoCorrect}
                    onFocus={this.props.onFocus}
                    onBlur={this.props.onBlur}
                >
                </TextInput>
                {this.props.suffixIcon}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    inputLayout: {
        flexDirection:'row',
        alignItems: 'center',
        paddingLeft: 12,
        borderBottomWidth: 0.5,
        borderBottomColor: '#999999'
    },
    inputField: {
    }
})
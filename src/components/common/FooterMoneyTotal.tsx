import * as React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import FontStyles from '../common/TextStyles'
import BigMoneyLayout from './BigMoneyLayout'

interface Props {
    amount: string
    color?: string
}

export default class FooterMoneyTotal extends React.Component<Props, undefined> {
    render() {
        return (
            <View style={styles.moneyDisplay}>
                <Text style={FontStyles.smallDark}>Total:</Text>
                <BigMoneyLayout
                    amount={this.props.amount}
                    color={this.props.color ? this.props.color : '#f26f21'}
                ></BigMoneyLayout>
                <Text style={styles.spacerText}>Total</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    moneyDisplay: {
        flexDirection: 'row',
        height: 60,
        backgroundColor: '#efefef',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        paddingVertical: 9
    },
    spacerText: { // Only for positioning
        color: 'rgba(0,0,0,0)'
    }
})
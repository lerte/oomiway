import * as React from 'react'
import { View, Text, StyleSheet, TouchableHighlight } from 'react-native'

interface Props {
    onPress: Function
    title: string
    disabled?: boolean
}

export default class UIButton extends React.Component<Props,undefined>{  
    render(){  
        return(
            <View style={styles.buttonLayout}>
                <TouchableHighlight
                    underlayColor={ this.props.disabled? "#CCC" : "#e76300" }
                    style={[styles.primay, this.props.disabled?{backgroundColor: '#CCC'}:null]}
                    onPress = {()=>this.props.onPress()}
                >  
                    <Text style={styles.text}>
                        {this.props.title}
                    </Text>
                </TouchableHighlight>
            </View>
        )
    }  
}

const styles = StyleSheet.create({
    buttonLayout: {
        height: 50,
        marginTop: 40,
        alignSelf: 'center',  
        flexDirection: 'row',  
        alignItems: 'center',
        borderRadius: 4,
        backgroundColor: "#F26F21"
    },
    primay: {
        height: 50,
        width: '100%',
        alignSelf: 'center',  
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        backgroundColor: "#F26F21"
    },
    text: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center'
    }
})
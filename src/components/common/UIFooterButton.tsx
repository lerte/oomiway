import * as React from 'react'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'

interface Props {
    onPress?: any,
    title: string
}

export default class FooterButton extends React.Component<Props, undefined> {
    render() {
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                style={styles.button}
            >
                <Text style={styles.text}>{this.props.title}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        height: 50,
        backgroundColor: '#f26f21',
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        color: 'white',
        fontSize: 18
    }
})
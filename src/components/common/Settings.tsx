import * as React from 'react'
import { Text, TouchableOpacity, Platform } from 'react-native'

import { IconBackSvg, IconLogoutSvg, IconHeaderCartSvg } from '../common/IconsSvg'

const Back = (navigation)=> {
    return(
        <TouchableOpacity style={{paddingLeft: 15}} onPress={()=>navigation.goBack()}>
            <IconBackSvg />
        </TouchableOpacity>
    )
};

export class Badge extends React.Component<any, undefined> {
    render() {
        return(
            <Text numberOfLines={1} style={{
                    position:'absolute',
                    flexDirection: 'row',
                    top: Platform.OS == "android" ? 0 : -7.5,
                    right: 7.5,
                    height: 15,
                    width: this.props.count.toString().length>1 ? '100%' : 15,
                    borderRadius: 7.5,
                    fontSize: 12,
                    color: 'white',
                    overflow: 'hidden',
                    textAlign: 'center',
                    justifyContent: 'center',
                    backgroundColor: '#F26F21'}}>
                    {this.props.count}
            </Text>
        )
    }
}

export const Logout = (navigation)=> {
    return(
        <TouchableOpacity style={{paddingRight: 15}} onPress={()=>navigation.navigate('Login')}>
            <IconLogoutSvg />
        </TouchableOpacity>
    )
};

export const Cart = (navigation, amount)=>{
    return(
        <TouchableOpacity style={{position: 'relative', paddingRight: 15}} onPress={() => navigation.navigate('MyCart')}>
            <IconHeaderCartSvg /><Badge count={amount} />
        </TouchableOpacity>
    )
};

export const BasicHeader = (navigation, title:string, headerRight?:any)=> {
    return({
        'headerLeft': Back(navigation),
        'title': title,
        'headerRight': headerRight,

        headerMode: 'screen',
        headerStyle: {
            backgroundColor: '#EFEFEF',
            shadowOpacity: 0,
            elevation: 0
        },
        headerTitleStyle: {
            color: '#666',
            fontSize: 18,
            fontWeight: 'normal',
            alignSelf: 'center'
        },
        headerTintColor: '#666'
    })
}

export const TranslucentHeader = ()=> {
    return({
        headerLeft: null,
		headerStyle: {
			backgroundColor: '#FFFFFF',
			shadowOpacity: 0,
			elevation: 0
		}
    })
}
import * as React from 'react'
import { View, Modal, StyleSheet, Text, TouchableHighlight, TouchableWithoutFeedback } from 'react-native'

import { IconCheckModalSvg, IconCloseModalSvg } from './IconsSvg'

interface Props {
    header: string
    message: string
    isVisible: boolean
    onPress: any
    onRequestClose: any
}

export default class AlertModal extends React.Component<Props, undefined> {
    render() {
        return(
            <Modal
                visible={this.props.isVisible}
                transparent={true}
                onRequestClose={this.props.onRequestClose}
            >
                <View style={styles.container}>
                    <TouchableHighlight
                        style={styles.overlay}
                        onPress={this.props.onPress}
                    >
                        <View></View>
                    </TouchableHighlight>
                    <View style={styles.alertModal}>
                        <IconCheckModalSvg />
                        <Text style={styles.header}>{this.props.header}</Text>
                        <Text style={styles.message}>{this.props.message}</Text>
                    </View>

                    <TouchableWithoutFeedback
                        onPress={this.props.onPress}
                    >
                        <View style={styles.closeButton}>
                            <IconCloseModalSvg />
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        alignItems: 'center',
    },
    overlay: {
        height: '100%',
        width: '100%',
        backgroundColor: 'white',
        position: 'absolute'
    },
    alertModal: {
        height: 200,
        width: 315,
        backgroundColor: 'white',
        borderRadius: 6,
        alignItems: 'center',
        padding: 20,
        marginTop: 220
    },
    header: {
        color: '#f26f21',
        margin: 10,
        fontSize: 18
    },
    message: {
        marginTop: 30,
        textAlign: 'center',
        color: '#999999',
        fontSize: 15
    },
    closeButton: {
        position: 'absolute',
        bottom: 82
    }
})
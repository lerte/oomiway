import { StyleSheet } from 'react-native'

const FontStyles = StyleSheet.create({
    smallText: { // descriptions & sub text; see PAY or CART
        color: '#999999',
        fontSize: 15
    },
    smallDark: {
        color: '#666666',
        fontSize: 15
    },
    smallOrange: {
        color: '#f26f21',
        fontSize: 15
    },
    regText: { // names; see PAY or CART
        color: '#666666',
        fontSize: 18
    },
    orangeReg: {
        color: '#f26f21',
        fontSize: 18
    },
    lightReg: {
        color: '#999999',
        fontSize: 18
    },
    verySmallLight: {
        fontSize: 12,
        color: '#999999'
    },
    verySmallDark: {
        fontSize: 12,
        color: '#666666'
    },
    verySmallOrange: {
        fontSize: 12,
        color: '#f26f21'
    }
})

export default FontStyles
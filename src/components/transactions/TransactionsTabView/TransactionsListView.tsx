import * as React from 'react'
import { View, StyleSheet, Text, ListView } from 'react-native'
import { connect } from 'react-redux'

import TextStyles from '../../common/TextStyles'
import TransactionsListViewCell from './TransactionsListViewCell'

interface Props {
    transactionData: object
    filter: string
    startDate: string
    endDate: string
}

interface State {
    dataSource: any
}

function mapStateToProps(state) {return {transactionData: state.transactions.transactions}}
class TransactionsListView extends React.Component<Props, State> {
    constructor(props) {
        super(props);
        this._initData()
    }

    _initData() {
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
        this.state = { dataSource: ds.cloneWithRows(this.props.transactionData) }
    }

    _filterData(cat, startDate, endDate) {
        const start = Date.parse(startDate)
        const end = Date.parse(endDate)
        const category = this.props.transactionData['filter']((item) => {
            if (cat == "all" || item.category == cat) {
                const date = Date.parse(item.date)
                if ((date < end && date > start) || startDate == "" || endDate == "") {
                    return true
                }
            }
            return false
        })
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
        this.setState({dataSource: ds.cloneWithRows(category)})
    }

    componentWillReceiveProps(nextProps) {
        const { filter, startDate, endDate } = nextProps;
        switch (filter) {
            case "all":
                this._filterData("all", startDate, endDate);
                break;
            case "sale":
                this._filterData("sale", startDate, endDate);
                break;
            case "restock":
                this._filterData("restock", startDate, endDate);
                break;
            case "transfer":
                this._filterData("transfer", startDate, endDate);
                break;
            default:
                break
        }
    }

    _renderHeader() {
        return (
            <View style={styles.container}>
                <Text style={[TextStyles.regText,styles.text]}>Amount</Text>
                <Text style={[TextStyles.regText,styles.text]}>Balance</Text>
            </View>
        )
    }

    _renderRow(data) {
        return (
            <TransactionsListViewCell
                {...data}
            ></TransactionsListViewCell>
        )
    }

    render() {
        return(
            <ListView
                scrollEnabled = {true}
                enableEmptySections = {true}
                dataSource = {this.state.dataSource}
                renderHeader = {this._renderHeader}
                stickySectionHeadersEnabled = {true}
                renderRow = {this._renderRow}
            ></ListView>
        )
    }
}
export default connect(mapStateToProps)(TransactionsListView)

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    text: {
        marginTop: 35,
        marginBottom: 7,
        marginLeft: 54,
        marginRight: 10
    }
});
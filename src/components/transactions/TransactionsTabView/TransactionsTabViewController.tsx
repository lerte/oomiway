import * as React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

import { BasicHeader } from '../../common/Settings'
import FontStyles from '../../common/TextStyles'
import TransactionsListView from './TransactionsListView'

import { NavigationAction, NavigationScreenProp } from 'react-navigation'
import { connect } from 'react-redux'

import { 
    IconWalletCalendarSvg, 
    IconWalletAllSvg, 
    IconWalletSalesSvg, 
    IconWalletRestock, 
    IconWalletTransfer 
} from '../../common/IconsSvg'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
    startDate: string
    endDate: string
}

interface State {
    category: string
}

function mapStateToProps(state) {
    return {
        startDate: state.transactions.startDate,
        endDate: state.transactions.endDate
    }
}
class TransactionsTabViewController extends React.Component<Props, State> {
    static navigationOptions = ( {navigation} ) => BasicHeader(navigation, 'Wallet')

    constructor(props){
        super(props);
        this.state = {
            category: "all"
        };
    }

    _formatDate(datestring: string) { // from yyyy-mm-dd to mm/dd/yyyy
        const split = datestring.split('-');
        const formattedDate = `${split[1]}/${split[2]}/${split[0]}`
        return formattedDate
    }

    render () {
        const { navigate } = this.props.navigation;
        const { endDate, startDate } = this.props;
        const dateRange = startDate && endDate ?
                            `${this._formatDate(startDate)} - ${this._formatDate(endDate)}` :
                            "Showing all transactions";
        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <View style={styles.topHeader}>
                        <View>
                            <Text style={FontStyles.smallDark}>{dateRange}</Text>
                            <View style={styles.flexRow}>
                                <Text style={styles.moneyDesc}>Expenditure </Text><Text style={styles.moneyText}>$800</Text>
                            </View>
                        </View>
                        <View>
                            <Text></Text>
                            <View style={styles.flexRow}>
                                <Text style={styles.moneyDesc}>Income </Text><Text style={styles.moneyText}>$5,700</Text>
                            </View>
                        </View>
                        <TouchableOpacity
                            style={styles.calendarButton}
                            onPress={() => navigate('CalendarFilter')}
                        >
                            <IconWalletCalendarSvg />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.tabHeader}>
                        <View style={styles.tabItem} onTouchEnd={()=>this._updateStatus("all")}>
                            <View style={[styles.tabItemIcon, this.state.category == "all" ? styles.tabItemIconActive : {}]}>
                                <IconWalletAllSvg />
                            </View>
                            <Text style={FontStyles.smallDark}>All</Text>
                        </View>
                        <View style={styles.tabItem} onTouchEnd={()=>this._updateStatus("sale")}>
                            <View style={[styles.tabItemIcon, this.state.category == "sale" ? styles.tabItemIconActive : {}]}>
                                <IconWalletSalesSvg />
                            </View>
                            <Text style={FontStyles.smallDark}>Sales</Text>
                        </View>
                        <View style={styles.tabItem} onTouchEnd={()=>this._updateStatus("restock")}>
                            <View style={[styles.tabItemIcon, this.state.category == "restock" ? styles.tabItemIconActive : {}]}>
                                <IconWalletRestock />
                            </View>
                            <Text style={FontStyles.smallDark}>Restock</Text>
                        </View>
                        <View style={styles.tabItem} onTouchEnd={()=>this._updateStatus("transfer")}>
                            <View style={[styles.tabItemIcon, this.state.category == "transfer" ? styles.tabItemIconActive : {}]}>
                                <IconWalletTransfer />
                            </View>
                            <Text style={FontStyles.smallDark}>Transfers</Text>
                        </View>
                    </View>
                    <TransactionsListView
                        filter={this.state.category}
                        startDate={startDate}
                        endDate={endDate}
                    ></TransactionsListView>
                </View>
            </View>
        )
    }

    _updateStatus(category: string) {
        this.setState({category: category})
    }
}
export default connect(mapStateToProps)(TransactionsTabViewController)

const styles = StyleSheet.create({
	container:{
        flex: 1,
		backgroundColor: '#EFEFEF'
	},
	body: {
		flex: 1,
		borderRadius: 4,
		marginLeft: 5,
        marginRight: 5,
		alignItems: 'center',
        backgroundColor: '#FFFFFF',
		justifyContent: 'flex-start'
    },
    topHeader: {
        width: '100%',
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        height: 55,
        paddingLeft: 10,
        paddingRight: 10,
        shadowColor: '#CCC',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.5,
        shadowRadius: 1,
        elevation: 1
    },
    flexRow: {
        flexDirection:'row',
        marginTop: 8
    },
    moneyDesc: {
        fontSize: 12,
        color: '#999999'
    },
    moneyText: {
        fontSize: 12,
        color: '#F26F21'
    },
    calendarButton: {
        height: '100%',
        width: 40,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    tabHeader: {
        width: '100%',
        paddingLeft: 20,
        paddingRight: 20,
        flexDirection:'row',
        justifyContent: 'space-between',
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    tabItemIcon: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: 15,
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: '#CCC'
    },
    tabItemIconActive: {
        backgroundColor: '#F26F21'
    }
})
import * as React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import TextStyles from '../../common/TextStyles'

import { 
    IconWalletListSale, 
    IconWalletListRestock, 
    IconWalletListTransfer 
} from '../../common/IconsSvg'

export interface Props {
    category: string
    name: string
    date: string
    amount: string
    balance: string
}

export default class WalletListViewCell extends React.Component<Props, undefined> {
    render() {
        return (
            <View style={styles.cell}>
                <View style={styles.icon}>
                    {this._setItemIcon(this.props.category)}
                </View>
                <View style={styles.container}>
                    <View style={styles.info}>
                        <Text style={TextStyles.smallDark}>{this.props.name}</Text>
                        <Text style={styles.date}>{this.props.date}</Text>
                    </View>
                    <View style={styles.flexRow}>
                        <Text style={[TextStyles.smallText,styles.amount]}>{this.props.amount}</Text>
                        <Text style={[TextStyles.smallText,styles.balance]}>{this.props.balance}</Text>
                    </View>
                </View>
            </View>
        )
    }

    _setItemIcon(category: string){
        switch(category) {
            case "sale":
                return <IconWalletListSale />
            case "restock":
                return <IconWalletListRestock />
            case "transfer":
                return <IconWalletListTransfer />
            default:
                return null
        }
    }
}

const styles = StyleSheet.create({
    cell: {
        height: 70,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 8,
        paddingVertical: 15,
        borderBottomColor: '#EEEEEE',
        borderBottomWidth: 0.5
    },
    container: {
        flex: 1,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    icon: {
        marginRight: 15
    },
    info: {
    },
    date: {
        marginTop: 3,
        fontSize: 12,
        color: '#999'
    },
    flexRow: {
        width: '60%',
        flexDirection: 'row'
    },
    amount: {
         width: '30%'
    },
    balance: {
        marginLeft: 72,
        marginRight: 11
    }
})

import * as React from 'react'
import { TouchableOpacity, StyleSheet, Text } from 'react-native'

import TextStyles from '../../common/TextStyles'

interface Props {
    left?: boolean
    date?: string
    onPress?: any
    selected: boolean
}

export default class DatePickerButton extends React.Component<Props, undefined> {
    render() {
        let displayDate
        if (this.props.date != "") { // NaN
            displayDate = this.props.date
        } else {
            displayDate = '_ _ / _ _ / _ _ _ _'
        }

        return (
            <TouchableOpacity
                style={[styles.container,
                    this.props.left ?
                        {marginRight: 3, borderTopLeftRadius: 4} :
                        {borderTopRightRadius: 4},
                    this.props.selected ?
                        styles.orangeCont :
                        styles.regularCont
                ]}
                onPress={this.props.onPress}
            >
                <Text
                    style={[
                        TextStyles.verySmallDark,
                        styles.header,
                        this.props.selected ? {color: 'white'} : null
                    ]}>{this.props.left ? "Start Date" : "End Date"}</Text>
                <Text
                    style={[
                        TextStyles.regText,
                        this.props.selected ? {color: 'white'} : null
                    ]}>{displayDate}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 55,
        flex: 1,
        marginBottom: 3,
        alignItems: 'center',
    },
    header: {
        marginVertical: 5
    },
    regularCont: {
        backgroundColor: 'white'
    },
    orangeCont: {
        backgroundColor: '#f26f21'
    }
})
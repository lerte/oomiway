import * as React from 'react'
import { View, Picker, Animated, Button, TouchableOpacity, PanResponder } from 'react-native'

import { connect } from 'react-redux'
import { NavigationAction, NavigationScreenProp } from 'react-navigation'
import {BasicHeader} from '../../common/Settings'
import styles from './CalendarStyles'

import CalendarView from './CalendarView'
import DatePickerButton from './DatePickerButton'
import FooterButton from '../../common/UIFooterButton'

import { setStartDate, setEndDate } from '../../../redux/actions/index'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
    startDate: string
    endDate: string
    eventDates: Array<string>
    setStartDate: any
    setEndDate: any
}

interface State {
    startDate: string
    endDate: string
    monthPicker: string
    yearPicker: string
    datePickerVisible: boolean
    currentlySelectingStartDate: boolean
    currentMonth: number
    currentYear: number
    footerButtonVisible: boolean
    initialDateSelectionComplete: boolean
}

const pickerHeight = 250

function mapStateToProps(state) {
    let eventDates = [];
    for (let transaction of state.transactions.transactions) {
        eventDates.push(transaction.date);
    }
    return {
        startDate: state.transactions.startDate,
        endDate: state.transactions.endDate,
        eventDates: eventDates
    }
}
function mapDispatchToProps(dispatch) {
    return {
        setStartDate: (date) => {
            dispatch(setStartDate(date))
        },
        setEndDate: (date) => {
            dispatch(setEndDate(date))
        }
    }
}
class CalendarFilterViewController extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        const startDate = this.props.startDate.split('/');
        const startMonth = parseInt(startDate[1]);
        const startYear = parseInt(startDate[0]);


        console.log(this.props.eventDates);
        const date = new Date();
        const currentMonth = date.getMonth() + 1;
        const currentYear = date.getFullYear();
        this.state = {
            startDate: this.props.startDate,
            endDate: this.props.endDate,
            monthPicker: currentMonth.toString(),
            yearPicker: currentYear.toString(),
            datePickerVisible: false,
            currentlySelectingStartDate: true,
            currentMonth: startMonth || currentMonth,
            currentYear: startYear || currentYear,
            footerButtonVisible: false,
            initialDateSelectionComplete: false,
        }
    }

    static navigationOptions = ( {navigation} ) => BasicHeader(navigation, 'Calendar')
    private pickerAnimVal = new Animated.Value(0);
    private footerAnimVal = new Animated.Value(0);
    private panResponder;

    componentWillMount() {
        this.panResponder = PanResponder.create({
            onMoveShouldSetPanResponder: (_, gestureState) => {
                const dx = gestureState.dx;
                const dy = gestureState.dy;
                const d = Math.sqrt(dx*dx + dy*dy)
                return (d > 30)
            },

            onPanResponderRelease: (_, gestureState) => {
                const dx = gestureState.dx
                if (dx < -30) {
                    return this._goNextMonth()
                }
                if (dx > 30) {
                    return this._goPrevMonth()
                }
            }
        })
    }

    componentDidMount() {
        if (this.props.startDate && this.props.endDate) {
            this._showFooter()
        }
    }

    _goNextMonth() {
        const { currentMonth } = this.state
        if (currentMonth == 13) {
            this.setState({currentMonth: 1, currentYear: this.state.currentYear + 1})
        } else {
            this.setState({currentMonth: currentMonth + 1})
        }
    }

    _goPrevMonth() {
        const { currentMonth } = this.state
        if (currentMonth == 1) {
            this.setState({currentMonth: 13, currentYear: this.state.currentYear - 1})
        } else {
            this.setState({currentMonth: currentMonth - 1})
        }
    }

    _dayPressed(day) {
        const pressedDate = day.dateString;
        const { currentlySelectingStartDate, startDate, endDate } = this.state;

        if (currentlySelectingStartDate && !this.state.initialDateSelectionComplete) {
            this.setState({
                currentlySelectingStartDate: !currentlySelectingStartDate,
                initialDateSelectionComplete: true
            });
            this.setState({startDate: pressedDate})
        } else if ((startDate == pressedDate) && currentlySelectingStartDate) {
            this.setState({startDate: ""});
            this._hideFooter()
        } else if ((endDate == pressedDate) && !currentlySelectingStartDate) {
            this.setState({endDate: ""});
            this._hideFooter()
        } else if (!currentlySelectingStartDate) {
            this.setState({endDate: pressedDate});
            if (startDate) {
                this._showFooter()
            }
        } else if (currentlySelectingStartDate) {
            this.setState({startDate: pressedDate})
            if (endDate) {
                this._showFooter()
            }
        }
    }

    _formatDate(date) { // mm/dd/yy for date picker buttons
        const d = new Date(date);
        if (String(d.getMonth()).charAt(0) == "N") { // NaN
            return ""
        } else {
            return [this._pad(d.getMonth() + 1), this._pad(d.getDate()), d.getFullYear()].join('/')
        }
    }

    _toggleDatePicker() {
        this.state.datePickerVisible ? this._hideDatePicker() : this._showDatePicker()
    }

    _showDatePicker() {
        this.setState({datePickerVisible: true});
        Animated.timing(this.pickerAnimVal,
            {
                toValue: 1,
                duration: 500
            }
        ).start()
    }

    _hideDatePicker() {
        this.setState({
            datePickerVisible: false,
            currentMonth: parseInt(this.state.monthPicker),
            currentYear: parseInt(this.state.yearPicker)
        });
        Animated.timing(this.pickerAnimVal,
            {
                toValue: 0,
                duration: 500
            }
        ).start()
    }

    _showFooter() {
        this.setState({footerButtonVisible: true})
        Animated.timing(this.footerAnimVal,
            {
                toValue: 1,
                duration: 300
            }
        ).start()
    }

    _hideFooter() {
        if (this.state.footerButtonVisible) {
            this.setState({footerButtonVisible: false})
            Animated.timing(this.footerAnimVal,
                {
                    toValue: 0,
                    duration: 300
                }
            ).start()
        } else {
            return null
        }
    }

    _pad(n) {
        return (parseInt(n) < 10) ? "0" + n : n.toString()
    }

    _handleDateButtonPress(isStart: boolean) { // is it the start or end button?
        const selectingStart = this.state.currentlySelectingStartDate;
        if (isStart == selectingStart) {
            return null
        } else {
            this.setState({currentlySelectingStartDate: !selectingStart})
        }
    }

    _handleConfirmation() {
        this.props.setStartDate(this.state.startDate)
        this.props.setEndDate(this.state.endDate)
        this.props.navigation.goBack();
        return
    }

    render() {
        const showPicker = this.pickerAnimVal.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 0 - pickerHeight]
        });

        const showFooter = this.footerAnimVal.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -50]
        });

        return (
            <View style={styles.container}>
                <View style={styles.buttons}>
                    <DatePickerButton
                        left={true}
                        onPress={this._handleDateButtonPress.bind(this, true)}
                        selected={this.state.currentlySelectingStartDate}
                        date={this._formatDate(this.state.startDate)}
                    ></DatePickerButton>
                    <DatePickerButton
                        left={false}
                        onPress={this._handleDateButtonPress.bind(this, false)}
                        selected={!this.state.currentlySelectingStartDate}
                        date={this._formatDate(this.state.endDate)}
                    ></DatePickerButton>
                </View>
                <View style={styles.body} {...this.panResponder.panHandlers}>
                    <View style={styles.pickerBtnContainer} pointerEvents="box-none">
                        <TouchableOpacity
                            style={styles.pickerBtn}
                            onPress={this._toggleDatePicker.bind(this)}
                            activeOpacity={0.7}
                        ></TouchableOpacity>
                    </View>
                    <CalendarView
                        startDate={this.state.startDate}
                        endDate={this.state.endDate}
                        onDayPress={this._dayPressed.bind(this)}
                        currentlySelectingStartDate={this.state.currentlySelectingStartDate}
                        current={`${this.state.currentYear}-${this._pad((this.state.currentMonth).toString())}-01`}
                        eventDates={this.props.eventDates}
                        onMonthChange={(d) => this.setState({
                            currentMonth: d.month,
                            currentYear: d.year
                        })}
                    >
                    </CalendarView>
                </View>
                <Animated.View
                    style={[styles.pickerContainer,
                        {
                            transform: [{translateY: showPicker}]
                        }]}>
                    <View style={styles.pickerHeader}>
                        <Button
                            title="Done"
                            onPress={this._hideDatePicker.bind(this)}
                        />
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <Picker
                            style={styles.picker}
                            selectedValue={this.state.monthPicker}
                            onValueChange={(val) => this.setState({monthPicker: val})}
                        >
                            <Picker.Item label="January" value="1"></Picker.Item>
                            <Picker.Item label="February" value="2"></Picker.Item>
                            <Picker.Item label="March" value="3"></Picker.Item>
                            <Picker.Item label="April" value="4"></Picker.Item>
                            <Picker.Item label="May" value="5"></Picker.Item>
                            <Picker.Item label="June" value="6"></Picker.Item>
                            <Picker.Item label="July" value="7"></Picker.Item>
                            <Picker.Item label="August" value="8"></Picker.Item>
                            <Picker.Item label="September" value="9"></Picker.Item>
                            <Picker.Item label="October" value="10"></Picker.Item>
                            <Picker.Item label="November" value="11"></Picker.Item>
                            <Picker.Item label="December" value="12"></Picker.Item>
                        </Picker>
                        <Picker
                            style={styles.picker}
                            selectedValue={this.state.yearPicker}
                            onValueChange={(val) => this.setState({yearPicker: val})}
                        >
                            <Picker.Item label="2017" value="2017"></Picker.Item>
                            <Picker.Item label="2018" value="2018"></Picker.Item>
                        </Picker>
                    </View>
                </Animated.View>
                <Animated.View style={[{transform: [{translateY: showFooter}]}, styles.footer]}>
                    <FooterButton
                        title="Confirm"
                        onPress={() => this._handleConfirmation()}
                    ></FooterButton>
                </Animated.View>
            </View>
        )
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CalendarFilterViewController)
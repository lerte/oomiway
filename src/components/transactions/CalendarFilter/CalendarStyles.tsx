import { StyleSheet } from 'react-native'

const pickerHeight = 250
const styles = StyleSheet.create({
    container: {
        height: '100%',
    },
    bar: {
        height: 300,
        position: 'absolute',
        right: 0,
        backgroundColor: 'black',
        width: 20,
        zIndex: 2
    },
    buttons: {
        flexDirection: 'row',
        marginHorizontal: 5,
        borderRadius: 4
    },
    body: {
        flex: 2,
        backgroundColor: 'white',
        marginHorizontal: 5
    },
    pickerBtnContainer: {
        position: 'absolute',
        height: 50,
        width: '100%',
        zIndex: 2,
        alignItems: 'center'
    },
    pickerBtn: {
        width: '60%',
        height: '100%',
        backgroundColor: 'white',
        opacity: 0
    },
    pickerContainer: {
        position: 'absolute',
        bottom: (0 - pickerHeight),
        zIndex: 2,
        left: 0,
        right: 0,
        backgroundColor: '#cccccc',
        height: pickerHeight,
        marginHorizontal: 5,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
    },
    pickerHeader: {
        borderBottomWidth: 0.5,
        borderColor: '#999999',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingRight: 15,
        paddingVertical: 5,
        alignItems: 'center'
    },
    picker: {
        flex: 1,
        borderWidth: 0
    },
    footer: {
        position: 'absolute',
        bottom: -50,
        left: 0,
        right: 0
    }
})

export default styles
import * as React from 'react'
import { Calendar, LocaleConfig } from 'react-native-calendars'

interface Props {
    onDayPress: any
    startDate: string
    endDate: string
    currentlySelectingStartDate: boolean
    current?: string
    onMonthChange: any
    eventDates: Array<string>
}

// Settings for the calendars
LocaleConfig.locales['default'] = {
    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    monthNamesShort: ['Jan', 'Feb', 'Mar', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
    dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    dayNamesShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S']
};
LocaleConfig.defaultLocale = 'default';

export default class CalendarView extends React.Component<Props, undefined> {
    private _getDateRange(start, end, dates) {
        let selectedDates = dates;

        function selectDate(date) {
            if (selectedDates[date]) {
                selectedDates[date].selected = true
            } else {
                selectedDates[date] = {selected: true}
            }
        }

        function pad(s: string) {
            if (s.length == 1) {
                return "0" + s
            }
            return s
        }

        if (start != "" && end != "") {
            const startDate = new Date(start);
            const endDate = new Date(end);
            const dayLength = 1000 * 60 * 60 * 24;
            const timeDiff = (endDate.getTime() - startDate.getTime() + dayLength) / dayLength;

            for (let i = 0; i < timeDiff; i++) {
                const dateInMs = startDate.getTime() + dayLength * (i + 1);
                const date = new Date(dateInMs);
                const formattedDate = date.getFullYear() + '-'
                    + pad(String(date.getMonth() + 1)) + '-'
                    + pad(String(date.getDate()));
                selectDate(formattedDate);
            }
        } else if (start == "") {
            selectDate(end)
        } else {
            selectDate(start)
        }
        return dates
    }

    render() {
        const startDate = this.props.startDate.replace(/\//g, '-')
        const endDate = this.props.endDate.replace(/\//g, '-')
        const eventDates = this.props.eventDates;

        let dates = {}
        for (let i = 0; i < eventDates.length; i++) {
            dates[eventDates[i]] = {marked: true}
        }
        const selectedDates = this._getDateRange(startDate, endDate, dates)

        return (
            <Calendar
                theme={calendarTheme}
                hideExtraDays={true}
                hideArrows={false}
                markedDates={selectedDates}
                onDayPress={this.props.onDayPress}
                maxDate={this.props.currentlySelectingStartDate ? endDate : null}
                minDate={this.props.currentlySelectingStartDate ? null : startDate}
                current={this.props.current}
                onMonthChange={this.props.onMonthChange}
            ></Calendar>
        )
    }
}

const calendarTheme = {
    calendarBackground: '#fff',
    textSectionTitleColor: '#666666',
    selectedDayBackgroundColor: '#f26f21',
    selectedDayTextColor: '#fff',
    todayTextColor: '#f26f21',
    dayTextColor: '#666666',
    dotColor: '#f26f21',
    selectedDotColor: '#fff',
    arrowColor: '#999999',
    monthTextColor: '#f26f21',
    textDayFontSize: 18,
    textMonthFontSize: 18,
    textDayHeaderFontSize: 18
}
import * as React from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'

import { IconChevronRightSvg } from '../../common/IconsSvg'
import TextStyles from '../../common/TextStyles'

interface Props {
    onPress?: any
}

export default class WithdrawalSourceNav extends React.Component<Props, undefined> {
    render() {
        return (
            <TouchableOpacity style={styles.container} onPress={this.props.onPress}>
                <View style={styles.image}></View>
                <View style={styles.info}>
                    <Text style={TextStyles.regText}>Visa</Text>
                    <Text style={TextStyles.smallText}>xxxx xxxx xxxx 8443</Text>
                </View>
                <IconChevronRightSvg></IconChevronRightSvg>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 10,
        height: 70,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderColor: '#efefef'
    },
    image: {
        height: 50,
        width: 50,
        backgroundColor: '#efefef'
    },
    info: {
        flex: 1,
        justifyContent: 'space-between',
        paddingLeft: 10,
        height: 50
    },
})
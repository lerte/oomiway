import * as React from 'react'
import { View, StyleSheet, TextInput, Text } from 'react-native'

import TextStyles from '../../common/TextStyles'
import WithdrawalSourceNav from './WithdrawalSourceNav'
import FooterButton from '../../common/UIFooterButton'

import { NavigationAction, NavigationScreenProp } from 'react-navigation'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
}

interface State {
    withdrawInput: string
}

export default class WithdrawViewController extends React.Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {withdrawInput: ""}
    }

    _isValidWithdrawAmount(withdraw, wallet) {
        const match = (withdraw.toString()).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/)
        const decimals = Math.max(0, (match[1] ? match[1].length : 0) - (match[2] ? +match[2] : 0))

        if (withdraw > wallet || decimals > 2) {
            return false
        }
        return true
    }

    render() {
        const { navigate, state } = this.props.navigation
        const walletAmount = state.params.walletAmount
        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <View style={styles.header}>
                        <Text style={TextStyles.regText}>Withdrawal Source</Text>
                    </View>
                    <WithdrawalSourceNav onPress={() => navigate('UpdatePaymentMethods')}></WithdrawalSourceNav>
                    <View style={styles.amount}>
                        <Text style={TextStyles.regText}>Withdraw Amount</Text>
                        <View style={styles.inputLayout}>
                            <Text style={styles.currency}>$</Text>
                            <TextInput
                                style={styles.inputField}
                                onChangeText={(text) => this.setState({withdrawInput: text})}
                                value={this.state.withdrawInput}
                            ></TextInput>
                        </View>
                    </View>
                    <View style={styles.bottom}>
                        <Text style={TextStyles.smallText}>Oomiwallet Balance: ${walletAmount}</Text>
                    </View>
                </View>
                <FooterButton
                    title="Withdraw"
                    onPress={() => {
                        if (this._isValidWithdrawAmount(this.state.withdrawInput, walletAmount)) {
                            navigate('WithdrawSuccessful', {amount: this.state.withdrawInput})
                        } else {
                            console.log('Invalid withdraw amount')
                        }
                    }}
                ></FooterButton>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        height: '100%',
        borderRadius: 4,
        justifyContent: 'space-between'
    },
    body: {
        marginHorizontal: 5,
        backgroundColor: 'white',
        flex: 1
    },
    header: {
        height: 50,
        borderBottomWidth: 0.5,
        borderColor: '#efefef',
        justifyContent: 'center',
        paddingLeft: 10
    },
    amount: {
        padding: 10,
        borderBottomWidth: 0.5,
        borderColor: '#efefef'
    },
    inputLayout: {
        marginTop: 10,
        flexDirection: 'row'
    },
    currency: {
        color: '#666666',
        fontSize: 25,
        marginRight: 6
    },
    inputField: {
        flex: 1,
        fontSize: 25,
        color: '#666666'
    },
    bottom: {
        padding: 10
    }
})
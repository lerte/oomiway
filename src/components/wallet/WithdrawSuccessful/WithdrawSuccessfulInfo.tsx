import * as React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import TextStyles from '../../common/TextStyles'

interface Props {
    title: string
    desc: string
}

export default class WithdrawSuccessfulInfo extends React.Component<Props, undefined> {
    render() {
        return (
            <View style={styles.container}>
                <Text style={TextStyles.lightReg}>{this.props.title}</Text>
                <Text style={TextStyles.regText}>{this.props.desc}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 10,
        borderBottomWidth: 0.5,
        borderColor: '#efefef',
        height: 60,
        width: '100%'
    }
})
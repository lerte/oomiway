import * as React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import { connect } from 'react-redux'
import { withdrawFromWallet } from "../../../redux/actions/index";

import FooterButton from '../../common/UIFooterButton'
import WithdrawSuccessfulInfo from './WithdrawSuccessfulInfo'

import { NavigationAction, NavigationScreenProp } from 'react-navigation'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
    withdraw: any
}

function mapStateToProps() {
    return {}
}
function mapDispatchToProps(dispatch, props) {
    return {
        withdraw: () => {
            dispatch(withdrawFromWallet(props.navigation.state.params.amount))
        }
    }
}
class WithdrawSuccessfulViewController extends React.Component<Props, undefined> {
    componentDidMount() {
        this.props.withdraw(this.props.navigation.state.params.amount)
    }

    render() {
        const { goBack, state } = this.props.navigation
        const withdrawAmount = state.params.amount
        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <View style={styles.checkmark}></View>
                    <Text style={styles.text}>Withdraw Successful</Text>
                    <WithdrawSuccessfulInfo
                        title="From"
                        desc="Visa 8443"
                    ></WithdrawSuccessfulInfo>
                    <WithdrawSuccessfulInfo
                        title="Amount"
                        desc={`$${withdrawAmount}`}
                    ></WithdrawSuccessfulInfo>
                </View>
                <FooterButton
                    title="Done"
                    onPress={() => {
                        goBack(null)
                        goBack(null)
                    }}
                ></FooterButton>
            </View>
        )
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(WithdrawSuccessfulViewController)

const styles = StyleSheet.create({
    container: {
        height: '100%',
        justifyContent: 'space-between',
    },
    body: {
        flex: 1,
        backgroundColor: 'white',
        borderRadius: 4,
        marginHorizontal: 5,
        alignItems: 'center'
    },
    checkmark: {
        height: 90,
        width: 90,
        borderRadius: 90,
        backgroundColor: '#f26f21',
        marginTop: 40,
        marginBottom: 20
    },
    text: {
        fontSize: 18,
        color: '#666666',
        marginBottom: 50
    }
})
import * as React from 'react'
import { View, Text, StyleSheet } from 'react-native'

interface Props {
    amount: number
}

export default class WalletMoneyDisplay extends React.Component<Props, undefined> {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.outerLayer}></View>
                <View style={styles.middleLayer}></View>
                <View style={styles.innerLayer}>

                </View>
                <Text style={styles.text}>${this.props.amount}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 150,
        width: 150,
        borderRadius: 150,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 60
    },
    outerLayer: {
        height: 150,
        width: 150,
        borderRadius: 150,
        backgroundColor: '#cccccc',
        opacity: 0.14,
        position: 'absolute'
    },
    middleLayer: {
        height: 130,
        width: 130,
        borderRadius: 130,
        position: 'absolute',
        backgroundColor: '#cccccc',
        opacity: 0.36,
    },
    innerLayer: {
        height: 110,
        width: 110,
        borderRadius: 110,
        backgroundColor: '#cccccc',
    },
    text: {
        backgroundColor: 'transparent',
        position: 'absolute',
        zIndex: 1,
        fontSize: 30,
        color: '#666666'
    }
})
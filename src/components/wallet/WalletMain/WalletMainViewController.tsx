import * as React from 'react'
import { View, Text } from 'react-native'

import { connect } from 'react-redux'

import WalletMoneyDisplay from './WalletMoneyDisplay'
import BigButton from '../../common/UIBigButton'

import { NavigationAction, NavigationScreenProp } from 'react-navigation'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
    walletAmount: number
}

function mapStateToProps(state) {
    return {walletAmount: state.wallet.walletAmount}
}
class WalletMainViewController extends React.Component<Props, undefined> {
    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={{
                height: '100%',
                backgroundColor: 'white',
                marginHorizontal: 5,
                borderRadius: 4,
                paddingHorizontal: 30,
                alignItems: 'center'
            }}>
                <WalletMoneyDisplay amount={this.props.walletAmount}></WalletMoneyDisplay>
                <Text style={{
                    fontSize: 18,
                    color: '#666666',
                    marginTop: 30,
                    marginBottom: 10
                }}>
                    My Wallet
                </Text>
                <BigButton
                    title="Withdraw"
                    onPress={() => navigate('Withdraw', {walletAmount: this.props.walletAmount})}
                ></BigButton>
            </View>
        )
    }
}
export default connect(mapStateToProps)(WalletMainViewController)
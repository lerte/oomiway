import * as React from 'react'
import { View, StyleSheet, KeyboardAvoidingView, Platform } from 'react-native'

import UIBigButton from '../common/UIBigButton'
import UITextInput from '../common/UITextInput'
import AlertModal from '../common/UIAlertModal'
import { BasicHeader } from '../common/Settings'

import { IconOomiBuySvg, IconEmailSvg, IconUserSvg, IconLockSvg, } from '../common/IconsSvg'


interface State {
	email: string
	emailInputFocus?: boolean
	username: string
	usernameInputFocus?: boolean
	password: string
	passwordInputFocus?: boolean
	confirmPassword: string
	confirmPasswordInputFocus?: boolean
	successModalVisible: boolean
}

export default class SignUpViewController extends React.Component<any, State>{
	static navigationOptions = ( {navigation} ) => BasicHeader(navigation, 'Create Account')
	constructor(props) {
		super(props)
		this.state = { 
			email: '', 
			username: '',
			password: '',
			confirmPassword: '',
			successModalVisible: false 
		}
	}
	_canSubmit() {
		return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(this.state.email) &&
				this.state.username && this.state.password &&
				this.state.password==this.state.confirmPassword
	}
    render () {
        return (
			<View style={styles.container}>
				<KeyboardAvoidingView style={{width: '100%'}} behavior='padding' keyboardVerticalOffset={Platform.OS =='android' ? -200 : 70}>
					<View style={styles.radiuBox}>
						<IconOomiBuySvg />
						<UITextInput
							prefixIcon={<IconEmailSvg color={this.state.emailInputFocus?'#F26F21':'#666'} />}
							placeholder='Email Address'
							inputFieldStyle={styles.inputField}
							containerStyle={styles.inputLayout}
							autoFocus={true}
							onFocus={()=>{this.setState({emailInputFocus: true})}}
							onBlur={()=>{this.setState({emailInputFocus: false})}}
							onChangeText={(email) => this.setState({email})}
						></UITextInput>

						<UITextInput
							prefixIcon={<IconUserSvg color={this.state.usernameInputFocus?'#F26F21':'#666'} />}
							placeholder='Username'
							inputFieldStyle={styles.inputField}
							containerStyle={styles.inputLayout}
							onFocus={()=>{this.setState({usernameInputFocus: true})}}
							onBlur={()=>{this.setState({usernameInputFocus: false})}}
							onChangeText={(username) => this.setState({username})}
						></UITextInput>

						<UITextInput
							prefixIcon={<IconLockSvg color={this.state.passwordInputFocus?'#F26F21':'#666'} />}
							placeholder='Password'
							secureTextEntry={true}
							inputFieldStyle={styles.inputField}
							containerStyle={styles.inputLayout}
							onFocus={()=>{this.setState({passwordInputFocus: true})}}
							onBlur={()=>{this.setState({passwordInputFocus: false})}}
							onChangeText={(password) => this.setState({password})}
						></UITextInput>

						<UITextInput
							prefixIcon={<IconLockSvg color={this.state.confirmPasswordInputFocus?'#F26F21':'#666'} />}
							placeholder='Confirm Password'
							secureTextEntry={true}
							inputFieldStyle={styles.inputField}
							containerStyle={styles.inputLayout}
							onFocus={()=>{this.setState({confirmPasswordInputFocus: true})}}
							onBlur={()=>{this.setState({confirmPasswordInputFocus: false})}}
							onChangeText={(confirmPassword) => this.setState({confirmPassword})}
						></UITextInput>

						<UIBigButton 
							title='OK' 
							onPress={()=>this._canSubmit()?this._dealSubmit():null} 
							disabled={!this._canSubmit()}>
						</UIBigButton>

						<AlertModal
							header='Registered Successfully'
							message='Please go to your email for confirmation'
							isVisible={this.state.successModalVisible}
							onPress={() => this.setState({ successModalVisible: false })}
							onRequestClose={() => this.setState({ successModalVisible: false })}
						></AlertModal>

					</View>
				</KeyboardAvoidingView>
			</View>
        )
    }
    _dealSubmit(){
		this.setState({ successModalVisible: true })
    }
}

const styles = StyleSheet.create({
	container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#EFEFEF'
	},
	radiuBox: {
        height: '100%',
		borderRadius: 4,
        marginHorizontal: 5,
        paddingHorizontal: 33,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#FFFFFF'
	},
	inputLayout: {
		width: '100%',
		flexDirection:'row',
		alignItems: 'center',
		marginTop: 30,
		paddingLeft: 12,
		borderBottomWidth: 0.5,
        borderBottomColor: 'rgba(153,153,153,.8)'
	},
	inputField: {
		height: 40,
		marginLeft: 15,
		width: '80%'
	},
	textLayout: {
		marginTop: 20,
		width: '100%',
		flexDirection:'row',
		justifyContent: 'space-between'
	},
	text: {
		color: '#F26F21',
		fontSize: 15
	}
})
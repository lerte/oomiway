import * as React from 'react'
import { Platform, KeyboardAvoidingView, View, Text, StyleSheet,TouchableOpacity } from 'react-native'
import {NavigationAction, NavigationScreenProp} from 'react-navigation'

import UIBigButton from '../common/UIBigButton'
import UITextInput from '../common/UITextInput'
import {TranslucentHeader} from '../common/Settings'

import { IconLoginLogoSvg, IconEmailSvg, IconLockSvg, IconEyeOpenSvg, IconEyeCloseSvg } from '../common/IconsSvg'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
}

interface State {
	email: string
	emailInputFocus?: boolean
	password: string
	passwordInputFocus?: boolean
	showPassword: boolean
}

export default class LoginViewController extends React.Component<Props, State> {
	static navigationOptions = ()=> TranslucentHeader()
	constructor(props) {
		super(props)
		this.state = { email: '', password: '', showPassword: false }
	}
	_canSubmit() {
		//return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(this.state.email) && this.state.password
		return true
	}
	render() {
		return (
			<KeyboardAvoidingView style={styles.container} behavior='padding' keyboardVerticalOffset={Platform.OS =='android' ? -200 : 70}>		
				<IconLoginLogoSvg />
				<UITextInput
					prefixIcon={<IconEmailSvg color={this.state.emailInputFocus?'#F26F21':'#666'} />}
					placeholder="Email Address"
					inputFieldStyle={styles.inputField}
					containerStyle={styles.inputLayout}
					autoFocus={true}
					onFocus={()=>{this.setState({emailInputFocus: true})}}
					onBlur={()=>{this.setState({emailInputFocus: false})}}
					onChangeText={(email) => this.setState({email})}
				></UITextInput>
				<UITextInput
					prefixIcon={<IconLockSvg color={this.state.passwordInputFocus?'#F26F21':'#666'} />}
					placeholder="Password"
					secureTextEntry={!this.state.showPassword}
					inputFieldStyle={styles.inputField}
					containerStyle={styles.inputLayout}
					onFocus={()=>{this.setState({passwordInputFocus: true})}}
					onBlur={()=>{this.setState({passwordInputFocus: false})}}
					onChangeText={(password) => this.setState({password})}
					suffixIcon={
						<TouchableOpacity onPress={()=>this.setState({showPassword: !this.state.showPassword})}>
							{ this.state.showPassword ? <IconEyeOpenSvg /> : <IconEyeCloseSvg /> }
						</TouchableOpacity>
					}
				></UITextInput>
				<UIBigButton 
					title='Log In' 
					onPress={()=>this._canSubmit()?this._dealPressLogin():null} 
					disabled={!this._canSubmit()}>
				</UIBigButton>
				<View style={styles.textLayout}>
					<Text style={styles.text} onPress={()=>this._go2ForgotPassword()}>Forgot Password</Text>
					<Text style={styles.text} onPress={()=>this._go2CreateAccount()}>Create Account</Text>
				</View>
			</KeyboardAvoidingView>
		)
	}
	_dealPressLogin(){
		this.props.navigation.navigate('Home')
	}
	_go2ForgotPassword(){
		this.props.navigation.navigate('RecoverPassword')
	}
	_go2CreateAccount(){
		this.props.navigation.navigate('SignUp')
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingHorizontal: 38,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'white'
	},
	inputLayout: {
		width: '100%',
		marginTop: 30,
		paddingLeft: 12,
		paddingRight: 12
	},
	inputField: {
		height: 40,
		marginLeft: 15,
		width: '80%'
	},
	textLayout: {
		marginTop: 20,
		width: '100%',
		flexDirection:'row',
		justifyContent: 'space-between'
	},
	text: {
		color: '#F26F21',
		fontSize: 15
	}
})
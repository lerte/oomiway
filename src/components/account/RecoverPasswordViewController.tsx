import * as React from 'react'
import { View, Text, StyleSheet, KeyboardAvoidingView, Platform } from 'react-native'

import { IconEmailSvg } from '../common/IconsSvg'
import UIBigButton from '../common/UIBigButton'
import UITextInput from '../common/UITextInput'
import AlertModal from '../common/UIAlertModal'
import {BasicHeader} from '../common/Settings'

interface State {
    email: string
    emailInputFocus?: boolean
    successModalVisible: boolean
}

export default class RecoverPasswordViewController extends React.Component<any, State>{
    static navigationOptions = ( {navigation} ) => BasicHeader(navigation, 'Recover Password');

    constructor(props) {
		super(props)
		this.state = { email: '', successModalVisible: false }
    }
    _canSubmit() {
		return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(this.state.email)
	}
    render () {
        return(
            <View style={styles.container}>
                <KeyboardAvoidingView style={{width: '100%'}} behavior='padding' keyboardVerticalOffset={Platform.OS =='android' ? -200 : 70}>
                    <View style={styles.radiuBox}>
                        <Text style={styles.text}>We will send you instructions to reset your password.</Text>
                        
                        <UITextInput
                            prefixIcon={<IconEmailSvg color={this.state.emailInputFocus?'#F26F21':'#666'} />}
                            placeholder='Email Address'
                            inputFieldStyle={styles.inputField}
                            containerStyle={styles.inputLayout}
                            autoFocus={true}
                            onFocus={()=>{this.setState({emailInputFocus: true})}}
						    onBlur={()=>{this.setState({emailInputFocus: false})}}
                            onChangeText={(email) => this.setState({email})}
                        ></UITextInput>

                        <UIBigButton 
                            title='Confirm'
                            onPress={()=>this._canSubmit()?this._dealPressConfim():null} 
                            disabled={!this._canSubmit()}>
                        </UIBigButton>

                        <AlertModal
                            header='Send Successfully'
                            message='Please go to your mailbox to reset your password'
                            isVisible={this.state.successModalVisible}
                            onPress={() => this.setState({ successModalVisible: false })}
                            onRequestClose={() => this.setState({ successModalVisible: false })}
                        ></AlertModal>
                    </View>
                </KeyboardAvoidingView>
            </View> 
        )
    }
    _dealPressConfim(){
        this.setState({ successModalVisible: true })
    }
}

const styles = StyleSheet.create({
	container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#EFEFEF'
	},
	radiuBox: {
        height: '100%',
		borderRadius: 4,
        marginHorizontal: 5,
        paddingHorizontal: 33,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#FFFFFF'
	},
    text: {
        color: '#999',
        fontSize: 18
    },
	inputLayout: {
		width: '100%',
		flexDirection:'row',
		alignItems: 'center',
		marginTop: 30,
		paddingLeft: 12,
		borderBottomWidth: 0.5,
        borderBottomColor: 'rgba(153,153,153,.8)'
	},
	inputField: {
		height: 40,
		marginLeft: 15,
		width: '80%'
	}
})
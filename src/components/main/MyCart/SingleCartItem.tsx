import * as React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import { connect } from 'react-redux'
import { addToCart, removeFromCart } from '../../../redux/actions/index'

import TextStyles from '../../common/TextStyles'
import CartButtonGroup from './CartButtonGroup'

interface Props {
    product: any
    incr(): void
    decr(): void
}

interface State {
    quantity: number
}

function mapDispatchToProps(dispatch, props) {
    const productId = props.product.id;
    return {
        incr: () => {
            dispatch(addToCart(productId))
        },
        decr: () => {
            dispatch(removeFromCart(productId))
        }
    }
}
class SingleCartItem extends React.Component<Props, State> {
    render() {
        const { name, price, productId, desc, quantity } = this.props.product;
        const { incr, decr } = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.thumbnail}></View>
                <View style={styles.info}>
                    <View style={styles.desc}>
                        <View style={styles.flexRow}>
                            <Text style={TextStyles.regText}>{name}</Text>
                        </View>
                        <Text
                            style={[TextStyles.smallText, styles.smallDesc]}
                            numberOfLines={1}
                        >{desc}
                        </Text>
                    </View>
                    <View style={styles.flexRow}>
                        <Text style={TextStyles.orangeReg}>${(price * 1).toFixed(2)}</Text>
                        <CartButtonGroup
                            productId={productId}
                            plusAction={() => incr()}
                            minusAction={() => decr()}
                            quantity={quantity}
                        ></CartButtonGroup>
                    </View>
                </View>
            </View>
        )
    }
}
export default connect(undefined, mapDispatchToProps)(SingleCartItem)

const styles = StyleSheet.create({
    container: {
        height: 130,
        backgroundColor: 'white',
        flexDirection: 'row',
        marginHorizontal: 5,
        marginTop: 5,
        paddingHorizontal: 8,
        paddingVertical: 22.5,
        borderRadius: 4
    },
    thumbnail: {
        height: 95,
        width: 95,
        backgroundColor: '#dddddd',
        borderRadius: 4,
        marginRight: 8
    },
    info: {
        width: '70%',
        justifyContent: 'space-between',
        height: 95
    },
    flexRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end'
    },
    desc: {
        height: 45,
        justifyContent: 'space-between'
    },
    smallDesc: {
        width: '75%'
    }
})

import * as React from 'react'
import { View } from 'react-native'

import CartListView from './CartListView'
import FooterButton from '../../common/UIFooterButton'
import FooterMoneyTotal from '../../common/FooterMoneyTotal'

import { BasicHeader } from '../../common/Settings'
import { connect } from 'react-redux'
import { NavigationAction, NavigationScreenProp } from 'react-navigation'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
    amount: number
}

function mapStateToProps(state) {
    let sum = 0; // Total price
    const cartItems = state.cart;
    for (let item in cartItems) {
        sum += (cartItems[item].quantity * cartItems[item].price);
    }

    return {amount: sum}
}
class MyCartViewController extends React.Component<Props, undefined> {
    static navigationOptions = ( {navigation} ) => BasicHeader(navigation, 'Create Account')

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={{height: '100%'}}>
                <CartListView></CartListView>
                <FooterMoneyTotal
                    amount={(this.props.amount.toFixed(2)).toString()}
                ></FooterMoneyTotal>
                <FooterButton
                    title="Confirm"
                    onPress={() => navigate('OrderConfirm')}
                >
                </FooterButton>
            </View>
        )
    }
}
export default connect(mapStateToProps)(MyCartViewController)
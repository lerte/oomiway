import * as React from 'react'
import { ListView } from 'react-native'
import { connect } from 'react-redux'

import SingleCartItem from './SingleCartItem'

interface Props {
    products: object
}

interface State {
    dataSource: any
}

function mapStateToProps(state) {return {products: state.cart}}
class CartListView extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
        this.state = {dataSource: ds.cloneWithRows(this.props.products)}
    }

    _renderRow(data) {
        return (
            <SingleCartItem
                product={data}
            ></SingleCartItem>
        )
    }

    render() {
        return(
            <ListView
                scrollEnabled={true}
                dataSource={this.state.dataSource}
                renderRow={this._renderRow}
            ></ListView>
        )
    }
}
export default connect(mapStateToProps)(CartListView)
import * as React from 'react'
import { View, TouchableOpacity, StyleSheet, Text } from 'react-native'

import { IconSmallPlusSvg, IconSmallMinusSvg } from '../../common/IconsSvg'
import TextStyles from '../../common/TextStyles'

interface Props {
    productId: number
    quantity: number
    plusAction(): void
    minusAction(): void
}

export default class CartQuantityButtonGroup extends React.Component<Props, undefined> {
    render() {
        const { quantity, minusAction, plusAction } = this.props;

        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.el}
                    onPress={minusAction}
                >
                    <IconSmallMinusSvg></IconSmallMinusSvg>
                </TouchableOpacity>
                <View style={[styles.el, styles.mid]}>
                    <Text
                        style={[
                            TextStyles.orangeReg,
                            quantity == 0 ? {color: '#666666'} : null
                        ]}
                    >
                        {quantity}
                    </Text>
                </View>
                <TouchableOpacity
                    style={styles.el}
                    onPress={plusAction}
                >
                    <IconSmallPlusSvg></IconSmallPlusSvg>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        borderColor: '#cccccc',
        borderWidth: 0.5,
        borderRadius: 4,
        height: 35,
        width: 130,
        flexDirection: 'row'
    },
    el: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    mid: {
        borderColor: '#cccccc',
        borderLeftWidth: 0.5,
        borderRightWidth: 0.5
    }
});
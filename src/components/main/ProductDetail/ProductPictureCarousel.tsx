import * as React from 'react'
import { Animated, View, StyleSheet, Image, Dimensions, ScrollView } from 'react-native'

const deviceWidth = Dimensions.get('window').width - 10 // 10 for margins
const spaceBetweenCircles = 20
const circleSize = 8

const images: Array<string> = [
    'https://via.placeholder.com/350x150/ffffff',
    'https://via.placeholder.com/350x200/ffffff',
    'https://via.placeholder.com/400x200/ffffff',
    'https://via.placeholder.com/400x100/ffffff',
];

export default class ProductPictureCarousel extends React.Component<any, any> {
    private numItems: number = images.length;
    private itemWidth: number = (circleSize / this.numItems) - ((this.numItems - 1) * spaceBetweenCircles);
    private animVal = new Animated.Value(0);

    public render() {
        let imageArray = [];
        let circleArray = [];

        images.forEach((image, i) => {
            const thisImage = (
                <Image
                    key={`image${i}`}
                    source={{uri: image}}
                    style={{width: deviceWidth}}
                />
            );
            imageArray.push(thisImage);

            const scrollBarVal = this.animVal.interpolate({
                inputRange: [deviceWidth * (i - 1), deviceWidth * (i + 1)],
                outputRange: [this.itemWidth, -this.itemWidth],
                extrapolate: 'clamp'
            });

            const thisCircle = (
                <View
                    key={`circle${i}`}
                    style={[
                        styles.circle,
                        {
                            width: circleSize,
                            height: circleSize,
                            borderRadius: circleSize,
                            marginLeft: i === 0 ? 0 : spaceBetweenCircles
                        }
                        ]}
                >
                    <Animated.View
                        style={[
                            styles.selected,
                            {
                                width: circleSize,
                                height: circleSize,
                                borderRadius: circleSize,
                                transform: [{translateX: scrollBarVal}]
                            }
                        ]}
                    />
                </View>
            );
            circleArray.push(thisCircle)
        });

        return (
            <View style={styles.container}>
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    pagingEnabled={true}
                    scrollEventThrottle={10}
                    onScroll={
                        Animated.event(
                            [{nativeEvent: {contentOffset: {x: this.animVal}}}]
                        )
                    }
                >
                    {imageArray}
                </ScrollView>
                <View style={styles.circleContainer}>
                    {circleArray}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 245,
        alignItems: 'center',
        justifyContent: 'center',
    },
    circle: {
        overflow: 'hidden',
        backgroundColor: '#cccccc',
    },
    selected: {
        position: 'absolute',
        left: 0,
        top: 0,
        backgroundColor: '#f26f21',
    },
    circleContainer: {
        position: 'absolute',
        zIndex: 2,
        bottom: 20,
        flexDirection: 'row'
    }
})
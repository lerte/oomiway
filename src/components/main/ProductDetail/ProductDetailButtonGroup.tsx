import * as React from 'react'
import { View, TouchableOpacity, StyleSheet, Text } from 'react-native'

import { IconLargePlusSvg, IconLargeMinusSvg } from '../../common/IconsSvg'

interface Props {
    productId: number
    quantity: number
    plusAction: any
    minusAction: any
}

export default class CartQuantityButtonGroup extends React.Component<Props, undefined> {
    render() {
        const { quantity, plusAction, minusAction } = this.props;

        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.el}
                    onPress={minusAction}
                >
                    <IconLargeMinusSvg></IconLargeMinusSvg>
                </TouchableOpacity>
                <View style={[styles.el, styles.mid]}>
                    <Text
                        style={[
                            styles.number,
                            quantity == 0 ? {color: '#666666'} : null
                        ]}
                    >
                        {quantity}
                    </Text>
                </View>
                <TouchableOpacity
                    style={styles.el}
                    onPress={plusAction}
                >
                    <IconLargePlusSvg></IconLargePlusSvg>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        borderWidth: 0.5,
        borderRadius: 4,
        flexDirection: 'row',
        height: 50,
        width: 180,
        borderColor: '#f26f21'
    },
    el: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#f26f21',
    },
    mid: {
        borderLeftWidth: 0.5,
        borderRightWidth: 0.5,
    },
    number: {
        fontSize: 20,
        color: '#f26f21'
    }
})
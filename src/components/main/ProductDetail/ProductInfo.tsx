import * as React from 'react'
import { View, Text, TouchableWithoutFeedback, StyleSheet } from 'react-native'

import TextStyles from '../../common/TextStyles'

interface Props {
    name: string,
    desc?: string,
    price: number
}

interface State {
    descExpanded: boolean
}

export default class ProductInfo extends React.Component<Props, State> {
    constructor(props) {
        super(props);
        this.state = {descExpanded: false}
    }

    private _displayDynamicDesc(desc: string, truncated: boolean) {
        return (
            <View>
                <Text
                    numberOfLines={truncated ? 3 : null}
                    style={[TextStyles.smallText, styles.desc]}>{desc}
                </Text>
                <Text style={TextStyles.smallOrange}>{truncated ? "More" : "Less"}</Text>
            </View>
        )
    }

    render() {
        const { desc } = this.props;
        const { descExpanded } = this.state;
        return (
            <View >
                <Text style={TextStyles.lightReg}>My Inventory: 2</Text>
                <View style={styles.row}>
                    <Text style={[TextStyles.regText, styles.name]}>{this.props.name}</Text>
                    <Text style={TextStyles.smallOrange}>${(this.props.price).toFixed(2)}</Text>
                </View>
                <TouchableWithoutFeedback
                    onPress={() => this.setState({ descExpanded: !this.state.descExpanded })}
                >
                    {descExpanded
                        ? this._displayDynamicDesc(desc, false)
                        : this._displayDynamicDesc(desc, true)}
                </TouchableWithoutFeedback>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 8
    },
    name: {
        marginRight: 10,
    },
    desc: {
        marginTop: 8
    }
})
import * as React from 'react'
import { View, StyleSheet, ScrollView, LayoutAnimation } from 'react-native'

import { connect } from 'react-redux'
import { setCartAmount } from '../../../redux/actions/index'

import ProductPictureCarousel from './ProductPictureCarousel'
import ProductInfo from './ProductInfo'
import ProductDetailButtonGroup from './ProductDetailButtonGroup'
import FooterButton from '../../common/UIFooterButton'
import FooterMoneyTotal from '../../common/FooterMoneyTotal'
import { BasicHeader, Cart } from '../../common/Settings'

import { NavigationAction, NavigationScreenProp } from 'react-navigation'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
    quantity: number
    setCartAmount: any
    productId: number;
    incr: any
    decr: any
}

interface State {
    scrollDisabled: boolean
    tempQuantity: number
}

function mapStateToProps(state, props) {
    const productId = props.navigation.state.params.product['id'];
    const products = state.cart;

    // Get the total number of items in the cart
    let totalCartSum = 0;
    for (let product in products) {
        totalCartSum += products[product].quantity;
    }

    return {
        quantity: products[productId].quantity,
        navigation: {...props.navigation,
            state: {...props.navigation.state,
                params: {...props.navigation.state.params,
                    cartSum: totalCartSum
                }
            }
        }
    }
}
function mapDispatchToProps(dispatch) {
    return {
        setCartAmount: (product, amount) => {
            dispatch(setCartAmount(product, amount))
        }
    }
}
class ProductDetailViewController extends React.Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {
            scrollDisabled: true,
            tempQuantity: this.props.quantity
        }
    }

    static navigationOptions = ({ navigation }) => {
        return BasicHeader(
            navigation, 'MyCart', Cart(navigation, navigation.getParam('cartSum') || 0)
        )
    }

    private _handleContentSizeChange() {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring)
        this.setState({scrollDisabled: !this.state.scrollDisabled})
        const scrollView: any = this.refs.scroll; // type as "any" to prevent tsc error
        scrollView.scrollTo({x: 0, y: 0, animated: false})
    }

    private _handleConfirm() {
        const productId: number = this.props.navigation.state.params.product['id'];
        this.props.navigation.goBack();
        this.props.setCartAmount(productId, this.state.tempQuantity);
    }

    private _plusAction() {
        this.setState({tempQuantity: this.state.tempQuantity + 1});
    }

    private _minusAction() {
        const { tempQuantity } = this.state;
        if (tempQuantity > 0) {
            this.setState({tempQuantity: this.state.tempQuantity - 1});
        }
    }

    public render() {
        const productData = this.props.navigation.state.params.product;

        return (
            <View style={styles.container}>
                <ScrollView
                    ref="scroll"
                    contentContainerStyle={styles.body}
                    scrollEnabled={this.state.scrollDisabled}
                    onContentSizeChange={() => this._handleContentSizeChange()}
                >
                    <ProductPictureCarousel></ProductPictureCarousel>
                    <View style={styles.info}>
                        <ProductInfo
                            {...productData}
                        ></ProductInfo>
                        <View style={styles.center}>
                            <ProductDetailButtonGroup
                                productId={productData['id']}
                                plusAction={() => this._plusAction()}
                                minusAction={() => this._minusAction()}
                                quantity={this.state.tempQuantity}
                            ></ProductDetailButtonGroup>
                    </View>
                    </View>
                </ScrollView>
                <View>
                    <FooterMoneyTotal
                        amount={(this.state.tempQuantity * productData.price).toFixed(2) + ""}
                    ></FooterMoneyTotal>
                    <FooterButton
                        title="Add to Cart"
                        onPress={() => this._handleConfirm()}
                    ></FooterButton>
                </View>
            </View>
        )
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailViewController)

const styles = StyleSheet.create({
    container: {
        justifyContent: 'space-between',
        height: '100%'
    },
    body: {
        backgroundColor: 'white',
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        marginHorizontal: 5
    },
    info: {
        borderTopWidth: 0.5,
        paddingTop: 15,
        paddingHorizontal: 10,
        borderColor: '#efefef',
        paddingBottom: 50
    },
    center: {
        alignItems: 'center',
        marginTop: 20
    },
})
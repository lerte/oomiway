import * as React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import TextStyles from '../../common/TextStyles'

interface Props {
    icon?: any
    title: string
    message: string
}

export default class CustomerInfoLine extends React.Component<Props, undefined> {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.icon}>{this.props.icon}</View>
                <Text style={TextStyles.lightReg}>{this.props.title}:
                    <Text style={TextStyles.regText}> {this.props.message}</Text>
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginBottom: 20,
        alignItems: 'center'
    },
    icon: {
        marginRight: 10
    }
})
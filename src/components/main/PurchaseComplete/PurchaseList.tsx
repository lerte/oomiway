import * as React from 'react'
import { View } from 'react-native'

import OrderInfoListView from '../OrderConfirm/OrderInfoListView'

export default class PurchaseList extends React.Component {
    render() {
        return (
            <View style={{margin: 5, marginBottom: 0, borderRadius: 4, flex: 1, backgroundColor: 'white'}}>
                <OrderInfoListView
                    header="Purchase List"
                    purchaseCompleteView={true}
                ></OrderInfoListView>
            </View>
        )
    }
}
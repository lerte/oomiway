import * as React from 'react'
import { View, StyleSheet } from 'react-native'

import CustomerInfo from './CustomerInfo'
import PurchaseList from './PurchaseList'
import FooterButton from '../../common/UIFooterButton'

import { StackActions, NavigationAction, NavigationScreenProp, NavigationActions } from 'react-navigation'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
}

export default class PurchaseCompleteViewController extends React.Component<Props, undefined> {
    static navigationOptions = {
        header: <View style={{backgroundColor: '#EFEFEF', height: 32}}></View>
    }
    _goBackToHomeScreen() {
        return (
            this.props.navigation.dispatch(StackActions.reset(
                {
                    index: 0,
                    actions: [
                        NavigationActions.navigate({ routeName: 'Home' })
                    ]
                }
            ))
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <CustomerInfo></CustomerInfo>
                    <PurchaseList></PurchaseList>
                </View>
                <View style={styles.footer}>
                    <FooterButton
                        title="Back to OOMIWAY"
                        onPress={() => {
                            this._goBackToHomeScreen()
                        }}
                    ></FooterButton>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        justifyContent: 'space-between'
    },
    body: {
        flex: 1,
    },
    footer: {
    }
})
import * as React from 'react'
import { View, StyleSheet, Text } from 'react-native'

import TextStyles from '../../common/TextStyles'
import CustomerInfoLine from './CustomerInfoLine'

import { IconLargeCheckMarkSvg, IconWaypointSvg, IconPhoneSvg, IconUserSvg } from '../../common/IconsSvg'

export default class CustomerInfo extends React.Component<any, undefined> {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.orderComplete}>
                    <View style={styles.circle}>
                        <IconLargeCheckMarkSvg></IconLargeCheckMarkSvg>
                    </View>
                    <Text style={TextStyles.orangeReg}>Order Completed</Text>
                </View>
                <CustomerInfoLine
                    title="Customer"
                    message="Blue"
                    icon={<IconUserSvg />}
                ></CustomerInfoLine>
                <CustomerInfoLine
                    title="Telephone"
                    message="222 1111 1111"
                    icon={<IconPhoneSvg />}
                ></CustomerInfoLine>
                <CustomerInfoLine
                    title="Address"
                    message="No.1048 Harbin xiangfang District songlei mension 3 floor"
                    icon={<IconWaypointSvg />}
                ></CustomerInfoLine>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        padding: 20,
        paddingBottom: 0,
        marginHorizontal: 5,
        borderRadius: 4
    },
    orderComplete: {
        alignItems: 'center',
        marginBottom: 15
    },
    circle: {
        marginBottom: 10
    }
})
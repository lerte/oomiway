import * as React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import TextStyles from '../../common/TextStyles'

interface Props {
    title: string,
    style?: any
}

export default class OrderConfirmHeader extends React.Component<Props, undefined> {
    render() {
        return (
            <View style={[styles.container, this.props.style]}>
                <Text style={TextStyles.regText}>{this.props.title}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 55,
        backgroundColor: 'white',
        paddingLeft: 10,
        alignItems: 'center',
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderColor: '#efefef',
        borderTopRightRadius: 4,
        borderTopLeftRadius: 4
    }
})
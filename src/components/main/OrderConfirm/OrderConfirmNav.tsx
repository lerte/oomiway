import * as React from 'react'
import { TouchableOpacity, View, Text, StyleSheet } from 'react-native'

import TextStyles from '../../common/TextStyles'
import { IconChevronRightSvg } from '../../common/IconsSvg'

interface Props {
    icon?: any
    title: string
    content?: string
    onPress: any
}

export default class OrderConfirmInfoNav extends React.Component<Props, undefined> {
    render() {
        return (
            <TouchableOpacity
                style={styles.container}
                onPress={this.props.onPress}
            >
                    <View style={styles.infoContainer}>
                        <View style={styles.icon}>{this.props.icon}</View>
                        <View style={styles.content}>
                            <Text numberOfLines={2} style={TextStyles.lightReg}>{this.props.title}:
                                <Text style={TextStyles.regText}> {this.props.content}</Text>
                            </Text>
                        </View>
                        <View style={{paddingRight: 10}}>
                            <IconChevronRightSvg />
                        </View>
                    </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 70,
        borderBottomWidth: 0.5,
        borderColor: '#efefef',
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
    },
    infoContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    content: {
        flex: 1,
        paddingRight: 10,
        flexWrap: 'wrap',
        flexDirection: 'row'
    },
    icon: {
        height: 26,
        width: 26,
        marginRight: 10,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
import * as React from 'react'
import { View, StyleSheet, ListView, Text } from 'react-native'

import OrderSingleListItem from './OrderSingleListItem'
import OrderConfirmHeader from './OrderConfirmHeader'
import TextStyles from '../../common/TextStyles'

import { connect } from 'react-redux'

interface Props {
    purchaseCompleteView?: boolean // is it for the PurchaseCompleteViewController
    header: string
    products: object
}

interface State {
    dataSource: any
}

function mapStateToProps(state) {
    const cart = state.cart
    let products = {}
    for (let item in cart) {
        if (cart[item].quantity > 0) {
            products[item] = cart[item]
        }
    }
    return {products: products}
}
class OrderInfoListView extends React.Component<Props, State> {
    constructor(props) {
        super(props)

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
        this.state = {dataSource: ds.cloneWithRows(this.props.products)}
    }

    _renderRow(data) {
        return (
            <OrderSingleListItem
                style={this.props.purchaseCompleteView ? styles.purchaseListItem : {}}
                {...data}
            ></OrderSingleListItem>
        )
    }

    _showTotalAmount() {
        let sum = 0
        for (let e in this.props.products) {
            const item = this.props.products[e]
            sum += item.quantity * item.price
        }
        return (
            <View style={styles.totalAmount}>
                <Text style={TextStyles.lightReg}>Paid: </Text>
                <Text style={TextStyles.orangeReg}>${sum.toFixed(2)}</Text>
            </View>
        )
    }

    render() {
        return (
            <ListView
                renderHeader={() => {
                    return (
                        <OrderConfirmHeader
                            title={this.props.header}>
                        </OrderConfirmHeader>
                    )
                }}
                renderFooter={() => {
                    if (this.props.purchaseCompleteView) {
                        return this._showTotalAmount()
                    } else {
                        return null
                    }
                }}
                dataSource={this.state.dataSource}
                renderRow={this._renderRow.bind(this)}
            ></ListView>
        )
    }
}
export default connect(mapStateToProps)(OrderInfoListView)

const styles = StyleSheet.create({
    bigPadding: {
        paddingBottom: 130,
    },
    smPadding: {
        paddingBottom: 0
    },
    purchaseListItem: {
        borderBottomWidth: 0.5,
        borderColor: '#efefef',
        height: 60
    },
    totalAmount: {
        height: 65,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: 8
    }
})
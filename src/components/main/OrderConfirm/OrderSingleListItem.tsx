import * as React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import TextStyles from '../../common/TextStyles'
import { IconOrderConfirmBulbSvg, IconOrderConfirmAirSvg, IconOrderConfirmCubeSvg, IconOrderConfirmPlugSvg } from '../../common/IconsSvg'

interface Props {
    name: string,
    quantity: number,
    price: number
    style?: any
}

export default class OrderSingleListItem extends React.Component<Props, undefined> {
    _renderAppropriateIcon() {
        switch (this.props.name) {
            case "Air":
                return (<IconOrderConfirmAirSvg />)
            case "Bulb":
                return (<IconOrderConfirmBulbSvg />)
            case "Cube":
                return (<IconOrderConfirmCubeSvg />)
            case "Plug":
                return (<IconOrderConfirmPlugSvg />)
            default:
                return null
        }

    }

    render() {
        return(
            <View style={[styles.container, this.props.style]}>
                <View style={styles.left}>
                    <View style={styles.icon}>
                        {this._renderAppropriateIcon()}
                    </View>
                    <Text style={TextStyles.lightReg}>{this.props.name}</Text>
                </View>
                <View style={styles.right}>
                    <Text style={TextStyles.regText}>{this.props.quantity}</Text>
                    <Text style={TextStyles.orangeReg}>${(this.props.quantity * this.props.price).toFixed(2)}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 47.5,
        flexDirection: 'row',
        paddingHorizontal: 10,
        alignItems: 'center'
    },
    icon: {
        marginRight: 10,
        width: 27,
        height: 27,
        alignItems: 'center',
        justifyContent: 'center'
    },
    left: {
        flex: 4,
        flexDirection: 'row',
    },
    right: {
        flex: 3,
        flexDirection: 'row',
        justifyContent: 'space-between',
    }
})
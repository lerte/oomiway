import * as React from 'react'
import { View, StyleSheet, DeviceEventEmitter } from 'react-native'

import OrderConfirmHeader from './OrderConfirmHeader'
import OrderConfirmNav from './OrderConfirmNav'
import OrderInfoListView from './OrderInfoListView'
import FooterButton from '../../common/UIFooterButton'
import FooterMoneyTotal from '../../common/FooterMoneyTotal'
import AlertModal from '../../common/UIAlertModal'

import { IconPaymentMethodSvg, IconWaypointSvg } from '../../common/IconsSvg'
import { BasicHeader } from '../../common/Settings'

import { NavigationAction, NavigationScreenProp } from 'react-navigation'
import { connect } from 'react-redux'
import { withdrawFromWallet } from '../../../redux/actions/index'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
    defaultPaymentMethod: number
    paymentMethods: object
    defaultAddress: number
    addresses: object
    moneyTotal: number
    withdrawFromWallet: any
}

interface State {
    failureModalVisible: boolean
    selectedPaymentMethod: number
    selectedAddress: number
}

function mapStateToProps(state) {
    const cart = state.cart
    let sum = 0
    for (let item in cart) {
        sum += (cart[item].quantity * cart[item].price)
    }
    return {
        paymentMethods: state.paymentmethods.paymentMethods,
        defaultPaymentMethod: state.paymentmethods.default,
        addresses: state.addresses.addresses,
        defaultAddress: state.addresses.default,
        moneyTotal: sum
    }
}
function mapDispatchToProps(dispatch) {
    return {
        withdrawFromWallet: amount => {
            dispatch(withdrawFromWallet(amount))
        }
    }
}
class OrderConfirmViewController extends React.Component<Props, State> {
    static navigationOptions = ({ navigation }) => BasicHeader(navigation, 'Order Confirmation')
    constructor(props) {
        super(props)

        this.state = {
            failureModalVisible: false,
            selectedPaymentMethod: this.props.defaultPaymentMethod,
            selectedAddress: this.props.defaultAddress
        }
    }

    componentWillMount() {
        DeviceEventEmitter.addListener('paymentMethodSelected', this._selectPaymentMethod.bind(this))
        DeviceEventEmitter.addListener('addressSelected', this._selectAddress.bind(this))
    }

    _selectPaymentMethod(e) {
        this.setState({
            selectedPaymentMethod: e.selected
        })
    }

    _selectAddress(e) {
        this.setState({
            selectedAddress: e.selected
        })
    }

    _handleOrderPlacement() {
        const { navigate } = this.props.navigation
        this.props.withdrawFromWallet(this.props.moneyTotal)
        navigate('PurchaseComplete')
        return
    }

    render() {
        const { navigate } = this.props.navigation
        const { address, city, state, zip, country } = this.props.addresses[this.state.selectedAddress]
        const fullAddress = `${address}, ${city}, ${state} ${zip}, ${country}`
        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <AlertModal
                        header="Payment Unsuccessful"
                        message="Try another payment method"
                        isVisible={this.state.failureModalVisible}
                        onPress={() => this.setState({ failureModalVisible: false })}
                        onRequestClose={() => this.setState({ failureModalVisible: false })}
                    ></AlertModal>
                    <OrderConfirmHeader title="Order Information"></OrderConfirmHeader>
                    <OrderConfirmNav
                        title="Payment Method"
                        content={this.props.paymentMethods[this.state.selectedPaymentMethod].title}
                        icon={<IconPaymentMethodSvg />}
                        onPress={() => navigate('PaymentMethods')}
                    ></OrderConfirmNav>
                    <OrderConfirmNav
                        title="Address"
                        content={fullAddress}
                        icon={<IconWaypointSvg />}
                        onPress={() => navigate('SelectAddress')}
                    ></OrderConfirmNav>
                    <OrderInfoListView
                        header="My Order"
                    ></OrderInfoListView>
                </View>
                <View>
                    <FooterMoneyTotal amount={this.props.moneyTotal.toFixed(2)}></FooterMoneyTotal>
                    <FooterButton
                        title="Continue"
                        onPress={() => this._handleOrderPlacement()}
                    ></FooterButton>
                </View>
            </View>
        )
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(OrderConfirmViewController)

const styles = StyleSheet.create({
    container: {
        justifyContent: 'space-between',
        height: '100%'
    },
    body: {
        backgroundColor: 'white',
        marginHorizontal: 5,
        borderRadius: 4,
        flex: 1
    }
})
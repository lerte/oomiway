import * as React from 'react'
import { ListView } from 'react-native'

import { connect } from 'react-redux'

import { BasicHeader, Cart } from '../../common/Settings'
import SmallProductView from './SmallProductView'
import BigProductView from "./BigProductView";

import { NavigationAction, NavigationActions, NavigationScreenProp } from 'react-navigation'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
    products: object
    cartSum: number
}

interface State {
    dataSource: any
}

function mapStateToProps(state) {
    let sum = 0
    for (let product in state.cart) {
        sum += state.cart[product].quantity
    }
    return {
        products: state.cart,
        cartSum: sum
    }
}
class ProductsViewController extends React.Component<Props, State> {
    constructor(props) {
        super(props)

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
        this.state = {
            dataSource: ds.cloneWithRows(this.props.products),
        }
    }

    static navigationOptions = ({ navigation }) => {
        return BasicHeader(
            navigation, 'Products', Cart(navigation, navigation.getParam('cartSum') || 0)
        )
    }

    componentDidMount() {
        const setParamsAction = NavigationActions.setParams({
            key: 'Products',
            params: {
                cartSum: this.props.cartSum
            }
        })
        this.props.navigation.dispatch(setParamsAction)
    }

    componentWillReceiveProps(nextProps) {
        const currentParams = this.props.navigation.state.params;
        const nextParams = nextProps.navigation.state.params;
        if (currentParams && nextParams) {
            if (currentParams.cartSum == nextParams.cartSum) {
                const setParamsAction = NavigationActions.setParams({
                    key: 'Products',
                    params: {
                        cartSum: nextProps.cartSum
                    }
                })
                this.props.navigation.dispatch(setParamsAction)
            }
        }
        this.setState({
            dataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            }).cloneWithRows(nextProps.products)
        })
    }

    _renderRow(data, _, rowID) {
        if (rowID == 1) {
            return this._renderBigProduct(data)
        } else {
            return this._renderSmallProduct(data)
        }
    }

    _renderBigProduct(productData: object) {
        const { navigate } = this.props.navigation;
        return (
            <BigProductView
                product={productData}
                onPress={() => navigate('ProductDetail', {product: productData})}
            />
        )
    }

    _renderSmallProduct(productData: object) {
        const { navigate } = this.props.navigation;
        return (
            <SmallProductView
                product={productData}
                onPress={() => navigate('ProductDetail', {product: productData})}
            ></SmallProductView>
        )
    }

    render() {
        return (
            <ListView
                contentContainerStyle={{
                    backgroundColor: '#EEEEEE',
                    alignItems: 'center',
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    paddingVertical: 3
                }}
                dataSource={this.state.dataSource}
                renderRow={this._renderRow.bind(this)}
            >
            </ListView>
        )
    }
}
export default connect(mapStateToProps)(ProductsViewController)
import * as React from 'react'

import ProductView from './ProductView'

interface Props {
    product: any // type checking in redux file
    onPress: any
}

export default class BigProductView extends React.Component<Props, undefined> {
    public render() {
        return (
            <ProductView
                onPress={this.props.onPress}
                product={this.props.product}
                big={true}
            ></ProductView>
        )
    }
}


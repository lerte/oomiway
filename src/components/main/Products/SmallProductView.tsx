import * as React from 'react'

import ProductView from './ProductView'

interface Props {
    product: any // type checking in redux file
    onPress: any
}

export default class SmallProductView extends React.Component<Props, undefined> {
    public render() {
        return (
            <ProductView
                product={this.props.product}
                onPress={this.props.onPress}
                big={false}
            />
        )
    }
}
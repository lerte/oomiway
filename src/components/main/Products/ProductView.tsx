// A superclass for Big and Small product view

import * as React from 'react'
import { View, StyleSheet, Text, TouchableOpacity, Image, Dimensions } from 'react-native'

import TextStyles from '../../common/TextStyles'

interface Props {
    product: any
    onPress: any
    big: boolean
}

export default class ProductView extends React.Component<Props, undefined> {
    private _getAppropriateImage() {
        switch (this.props.product.id) {
            case 1:
                return require('../../../../images/all/all_main.png')
            case 2:
                return require('../../../../images/cube/cube_main.png')
            case 3:
                return require('../../../../images/touch/touch_main.png')
            case 4:
                return require('../../../../images/bulb/bulb_main.png')
            case 5:
                return require('../../../../images/plug/plug_main.png')
            case 6:
                return require('../../../../images/multisensor/multisensor_main.png')
            case 7:
                return require('../../../../images/dock/dock_main.png')
            default:
                return null
        }
    }

    private _showAmountInCart() {
        return (
            <Text style={[styles.inCart, TextStyles.smallText]}>
                In Cart: <Text style={TextStyles.smallOrange}>{this.props.product.quantity}</Text>
            </Text>
        )
    }

    public render() {
        const stockDanger = this.props.product.stock <= 3;
        const name = this.props.product.name;

        const bigWidth  = (Dimensions.get('window').width - 10);
        const smallWidth  = (Dimensions.get('window').width - 15) / 2;
        const bigHeight = 205;
        const smallHeight = 170;

        return (
            <TouchableOpacity
                style={[styles.container,
                    this.props.big ?
                        {width: bigWidth, height: bigHeight} :
                        {width: smallWidth, height: smallHeight},
                ]}
                onPress={this.props.onPress}
            >
                <View style={[
                    styles.imgContainer,
                    name == "Cube" ? styles.cubeOffset : null,
                    name == "Plug" ? styles.plugOffset : null,
                    name == "Bulb" ? styles.bulbOffset : null,
                    name == "Touch" ? styles.touchOffset : null
                ]}
                >
                    <Image
                        source={this._getAppropriateImage()}
                    />
                </View>
                <View>
                    {this.props.product.quantity > 0 ? this._showAmountInCart() : null}
                </View>
                <View style={styles.info}>
                    <Text style={TextStyles.smallText}>{this.props.product.name}</Text>
                    <Text style={stockDanger ? TextStyles.verySmallOrange : TextStyles.verySmallLight}>Stock:
                        <Text style={stockDanger ? TextStyles.verySmallOrange : TextStyles.verySmallDark}> {this.props.product.stock}</Text>
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        borderRadius: 4,
        marginHorizontal: 5,
        marginBottom: 5,
        flexDirection: 'column',
        justifyContent: 'space-between',
        padding: 8,
        marginRight: 0
    },
    imgContainer: {
        position: 'absolute',
        right: 0,
        left: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cubeOffset: {
        paddingRight: 30
    },
    plugOffset: {
        paddingTop: 20,
        paddingRight: 10
    },
    bulbOffset: {
        paddingTop: 20
    },
    touchOffset: {
        paddingLeft: 5,
        paddingTop: 25
    },
    inCart: {
        right: 0,
        top: 0,
        flexDirection: 'row',
        position: 'absolute',
        backgroundColor: 'transparent'
    },
    info: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        backgroundColor: 'transparent'
    }
})
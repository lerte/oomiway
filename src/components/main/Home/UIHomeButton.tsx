import * as React from 'react'
import { View, Text, TouchableHighlight, StyleSheet } from 'react-native'

import { IconAttentionShadowSvg } from '../../common/IconsSvg'

interface Props {
    onPress?: any,
    onHideUnderlay?: any,
    onShowUnderlay?: any,
    width?: number,
    color?: string,
    icon?: any,
    badge?: any,
    title?: string,
}

export default class UIHomeButton extends React.Component<Props, undefined> {
    _showBadge() {
        return(
            <View style={styles.badge}>
                <IconAttentionShadowSvg></IconAttentionShadowSvg>
            </View>
        )
    }
    render() {
        return (
            <View>
                {this.props.badge?this._showBadge():null}
                <TouchableHighlight
                    style={[styles.button, {width:this.props.width}]}
                    onPress={this.props.onPress}
                    onHideUnderlay={this.props.onHideUnderlay}
                    onShowUnderlay={this.props.onShowUnderlay}
                    underlayColor={this.props.color}
                >
                    <View style={{alignItems: 'center'}}>
                        {this.props.icon}
                        <Text style={[styles.title,{color: this.props.color}]}>{ this.props.title }</Text>
                    </View>
                </TouchableHighlight>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 100,
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#F26F21'
    },
    badge: {
        zIndex: 1,
        position:'absolute',
        marginTop: -12.5,
        right: -12.5,
        width: 25,
        height: 25,
        borderRadius: 12.5,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: 'black',
        shadowRadius: 2,
        shadowOpacity: .5,
        shadowOffset: {width: 0, height: 2},
        elevation: 5
    },
	title: {
		color: '#F26F21',
		fontSize: 18
    }
})
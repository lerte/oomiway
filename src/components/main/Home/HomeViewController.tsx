import * as React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { NavigationAction, NavigationScreenProp } from 'react-navigation'

import { connect } from 'react-redux'

import UIHomeButton from './UIHomeButton'
import FontStyles from '../../common/TextStyles'
import { TranslucentHeader } from '../../common/Settings'

import { IconAvatarSvg, IconWalletSvg, IconRestockSvg, IconSellSvg } from '../../common/IconsSvg'


interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
    walletAmount: number
}

interface State {
    button: any
}

function mapStateToProps(state) {
    return {walletAmount: state.wallet.walletAmount}
}
class HomeViewController extends React.Component<Props, State> {
    static navigationOptions = _ => TranslucentHeader();
    constructor(props) {
        super(props);
        this.state={
            button: {}
        }
    }
    _onHideUnderlay(name: string) {
        this.state.button[name] = false
        this.setState(this.state.button)
    }
    _onShowUnderlay(name: string) {
        this.state.button[name] = true
        this.setState(this.state.button)
    }

    render () {
        return(
            <View style={styles.container}>
                <TouchableOpacity style={styles.avatarLayout} onPress={()=>this._go2Profile()}>
                    <IconAvatarSvg></IconAvatarSvg>
                </TouchableOpacity>
                <Text style={FontStyles.lightReg}>Blue</Text>
                <TouchableOpacity
                    style={styles.wallet}
                    onPress={() => this._go2Wallet()}
                >
                    <Text style={styles.moneyDisplay}>${this.props.walletAmount.toFixed(2)}</Text>
                    <Text style={styles.walletTitle}>My Wallet</Text>
                </TouchableOpacity>
                <View style={styles.buttons}>
                    <UIHomeButton
                        onPress={()=>this._go2Transactions()}
                        onHideUnderlay={()=>this._onHideUnderlay("wallet")}
                        onShowUnderlay={()=>this._onShowUnderlay("wallet")}
                        width={130}
                        color={this.state.button['wallet']?'#FFFFFF':'#F26F21'}
                        icon={<IconWalletSvg color={this.state.button['wallet']?'#FFFFFF':'#F26F21'} />}
                        title="Transaction"
                    >
                    </UIHomeButton>
                    <UIHomeButton
                        badge={true}
                        onPress={()=>this._go2Restock()}
                        onHideUnderlay={()=>this._onHideUnderlay('restock')}
                        onShowUnderlay={()=>this._onShowUnderlay('restock')}
                        width={130}
                        color={this.state.button['restock']?'#FFFFFF':'#F26F21'}
                        icon={<IconRestockSvg color={this.state.button['restock']?'#FFFFFF':'#F26F21'} />}
                        title="Restock"
                    >
                    </UIHomeButton>
                </View>
                <View style={styles.sell}>
                    <UIHomeButton
                        onPress={()=>this._go2Sell()}
                        onHideUnderlay={()=>this._onHideUnderlay('sell')}
                        onShowUnderlay={()=>this._onShowUnderlay('sell')}
                        color={this.state.button['sell']?'#FFFFFF':'#F26F21'}
                        icon={<IconSellSvg color={this.state.button['sell']?'#FFFFFF':'#F26F21'} />}
                        title="Sell"
                    >
                    </UIHomeButton>
                </View>
            </View>
        )
    }
    _go2Profile(){
		this.props.navigation.navigate('AccountProfile')
    }
    _go2Transactions(){
		this.props.navigation.navigate('Transactions')
    }
    _go2Restock(){
        this.props.navigation.navigate('Products')
    }
    _go2Sell(){
        this.props.navigation.navigate('Scanner')
    }
    _go2Wallet() {
        this.props.navigation.navigate('WalletMain')
    }
}
export default connect(mapStateToProps)(HomeViewController)

const styles = StyleSheet.create({
	container: {
		flex: 1,
        padding: 38,
        alignItems: 'center',
        justifyContent: 'space-between',
		backgroundColor: 'white'
    },
	avatarLayout: {
        backgroundColor: '#CCC',
        width: 80,
        height: 80,
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    wallet: {
        alignItems: 'center'
    },
    moneyDisplay: {
        fontSize: 30,
        color: '#f26f21'
    },
    walletTitle: {
        fontSize: 15,
        color: '#f26f21'
    },
    buttons: {
        width: '100%',
        flexDirection:'row',
		justifyContent: 'space-between'
    },
    sell: {
        width: '100%',
        justifyContent: 'center'
    }
})

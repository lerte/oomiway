import * as React from 'react'
import { View, DeviceEventEmitter } from 'react-native'

import FooterButton from '../../common/UIFooterButton'
import PaymentMethodsListView from './PaymentMethodListView'
import FooterMoneyTotal from '../../common/FooterMoneyTotal'

import { NavigationAction, NavigationScreenProp } from 'react-navigation'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
}

interface State {
    selectedRow: number
}

export default class PaymentMethodsViewController extends React.Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {selectedRow: null}
    }

    _getSelectedRow(row) {
        this.setState({selectedRow: row})
    }

    _handleSelection() {
        if (this.state.selectedRow) {
            this.props.navigation.goBack();
            // Emits to "OrderConfirmViewController"
            DeviceEventEmitter.emit('paymentMethodSelected', {selected: this.state.selectedRow})
        } else {
            console.log('Please select a payment method')
        }
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={{height: '100%'}}>
                <PaymentMethodsListView
                    addButtonPressed={() => navigate('AddCreditCard')}
                    onEditBtnPress={() => navigate('AddCreditCard', {edit: this.state.selectedRow})}
                    onRowPress={this._getSelectedRow.bind(this)}
                >
                </PaymentMethodsListView>
                <View>
                    <FooterMoneyTotal
                        amount="709.50"
                    ></FooterMoneyTotal>
                    <FooterButton
                        title="Select"
                        onPress={() => this._handleSelection()}
                    ></FooterButton>
                </View>
            </View>
        )
    }
}
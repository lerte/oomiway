import * as React from 'react'
import { View, TouchableOpacity, StyleSheet, Text } from 'react-native'

import TextStyles from '../../common/TextStyles'
import { IconVisaCardSvg, IconMasterCardSvg } from '../../common/IconsSvg'

interface Props {
    title: string // User can choose the name displayed; default is type (e.g. Visa)
    type?: string // Some indicator of which thumbnail to use
    number: string // e.g. last 4 digits of credit card number
    onPress: any
    selected: boolean
    onEditBtnPress: any
    onDeleteBtnPress: any
}

export default class SinglePaymentMethod extends React.Component<Props, undefined> {
    _renderSelected() {
        if (this.props.selected) {
            return (
                <View style={styles.selected}>
                    <TouchableOpacity style={styles.fnBtn} onPress={this.props.onEditBtnPress}>
                        <Text style={TextStyles.smallOrange}>Edit</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.fnBtn} onPress={this.props.onDeleteBtnPress}>
                        <Text style={TextStyles.smallOrange}>Delete</Text>
                    </TouchableOpacity>
                </View>
            )
        } else {
            return null
        }
    }

    _renderAppropriateIcon() {
        switch (this.props.type) {
            case "visa":
                return (<IconVisaCardSvg></IconVisaCardSvg>)
            case "mastercard":
                return (<IconMasterCardSvg></IconMasterCardSvg>)
            default:
                return null
        }
    }

    render() {
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                activeOpacity={0.8}
                style={[
                    styles.container,
                    this.props.selected ? styles.selHeight : styles.normHeight
                ]}
            >
                <View style={styles.info}>
                    <View style={styles.thumbnail}>{this._renderAppropriateIcon()}</View>
                    <View style={styles.infoText}>
                        <Text style={TextStyles.regText}>{this.props.title}</Text>
                        <Text style={TextStyles.smallText}>xxxx xxxx xxxx {this.props.number}</Text>
                    </View>
                </View>
                {this._renderSelected()}
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 30,
        paddingLeft: 15,
        marginTop: 5,
        borderRadius: 5,
        marginHorizontal: 5,
        backgroundColor: 'white'
    },
    selHeight: {
        height: 155,
        shadowColor: 'black',
        shadowRadius: 4,
        shadowOpacity: 0.3,
        shadowOffset: {width: 0, height: 0},
        elevation: 4,
        zIndex: 1,
        borderRadius: 4
    },
    normHeight: {
        height: 115,
    },
    activatedShadow: {
        shadowColor: 'black',
        shadowRadius: 4,
        shadowOpacity: 0.3,
        shadowOffset: {width: 0, height: 0}
    },
    info: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center'
    },
    checkbox: {
        height: 20,
        width: 20,
        borderRadius: 20,
        borderColor: '#999999',
        borderWidth: 1
    },
    thumbnail: {
        width: 50,
        height: 50,
        marginHorizontal: 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    infoText: {
        marginLeft: 15,
        justifyContent: 'space-between',
        flexDirection: 'column',
        height: 50
    },
    selected: {
        paddingHorizontal: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20
    },
    fnBtn: {
        height: 35,
        width: 85,
        borderColor: '#f26f21',
        borderWidth: 1,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

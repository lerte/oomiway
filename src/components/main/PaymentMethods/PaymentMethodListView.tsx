import * as React from 'react'
import { ListView, View, Alert, DeviceEventEmitter } from 'react-native'

import { connect } from 'react-redux'

import SinglePaymentMethod from './SinglePaymentMethod'
import AddButton from '../../common/UIAddButton'

import { deletePaymentMethod } from '../../../redux/actions/index'

interface Props {
    addButtonPressed: any
    paymentMethods: object
    onEditBtnPress: any
    onRowPress: any
    deletePaymentMethod: any
}

interface State {
    dataSource: any
    selectedRow: number
}

function mapStateToProps(state) {return {paymentMethods: state.paymentmethods.paymentMethods}}
function mapDispatchToProps(dispatch) {
    return {
        deletePaymentMethod: (index) => {
            dispatch(deletePaymentMethod(index))
        }
    }
}
class PaymentMethodListView extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1.selected !== r2.selected
        });

        this.state = {
            dataSource: ds.cloneWithRows(this.props.paymentMethods),
            selectedRow: null,
        }
    }

    componentWillMount() {
        DeviceEventEmitter.addListener('updatedPaymentMethods', () => this._refreshList())
    }

    private _refreshList() {
        this.setState({
            dataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1.selected !== r2.selected
            }).cloneWithRows(this.props.paymentMethods)
        })
    }

    private _rowPressed(rowID) {
        const { selectedRow } = this.state;
        let newSelectedRow;
        rowID == selectedRow ? newSelectedRow = null : newSelectedRow = rowID
        {this.props.onRowPress(newSelectedRow)}
        this.setState({
            dataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1.selected !== r2.selected
            }).cloneWithRows(this.props.paymentMethods),
            selectedRow: newSelectedRow
        })
    }

    private _renderRow(data, _, rowID) {
        return (
            <SinglePaymentMethod
                title={data.title}
                type={data.type}
                number={data.cardNum.substr(-4)}
                onPress={() => this._rowPressed(rowID)}
                selected={this.state.selectedRow == rowID}
                onEditBtnPress={this.props.onEditBtnPress}
                onDeleteBtnPress={() => this._sendDeleteAlert(data.title)}
            ></SinglePaymentMethod>
        )
    }

    private _sendDeleteAlert(name) {
        return Alert.alert(
            'Delete Payment Method?',
            `'${name}' will be deleted`,
            [
                {text: 'Cancel', onPress: () => {}, style: 'cancel'},
                {text: 'Ok', onPress: () => this._deleteRow(), style: 'default'},
            ],
            {cancelable: true}
        )
    }

    private _deleteRow() {
        this.props.deletePaymentMethod(this.state.selectedRow);
        this._refreshList()
    }

    render() {
        return (
            <ListView
                style={{borderRadius: 4}}
                dataSource={this.state.dataSource}
                renderRow={this._renderRow.bind(this)}
                renderFooter={() => {
                    return (
                        <View style={{alignItems: 'center'}}>
                            <AddButton
                                onPress={this.props.addButtonPressed}
                            ></AddButton>
                        </View>
                    )
                }}
            >
            </ListView>
        )
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PaymentMethodListView)
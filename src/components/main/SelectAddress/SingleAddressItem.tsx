import * as React from 'react'
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native'

import TextStyles from '../../common/TextStyles'

import { IconWaypointSvg } from '../../common/IconsSvg'

interface Props {
    address: string
    onPress: any
    selected: boolean
    onEditBtnPress: any
    onDeleteBtnPress: any
}

export default class SelectAddressListView extends React.Component<Props, undefined> {
    _renderSelected() {
        if (this.props.selected) {
            return (
                <View style={styles.selected}>
                    <TouchableOpacity style={styles.fnBtn} onPress={this.props.onEditBtnPress}>
                        <Text style={TextStyles.smallOrange}>Edit</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.fnBtn} onPress={this.props.onDeleteBtnPress}>
                        <Text style={TextStyles.smallOrange}>Delete</Text>
                    </TouchableOpacity>
                </View>
            )
        } else {
            return null
        }
    }

    render() {
        return (
            <TouchableOpacity
                style={[styles.container, this.props.selected ? styles.selHeight : null]}
                onPress={this.props.onPress}
                activeOpacity={0.8}
            >
                <View style={styles.body}>
                    <IconWaypointSvg></IconWaypointSvg>
                    <View style={styles.address}>
                        <Text
                            style={TextStyles.lightReg}
                            numberOfLines={this.props.selected ? null : 2}
                        >
                            Address:
                            <Text style={TextStyles.regText}>
                                {" "}{this.props.address}
                            </Text>
                        </Text>
                    </View>
                </View>
                {this._renderSelected()}
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        borderColor: '#efefef',
        paddingLeft: 10,
        paddingRight: 20,
        paddingVertical: 25,
        borderBottomWidth: 0.5,
        marginHorizontal: 5,
        backgroundColor: 'white',
    },
    selHeight: {
        borderRadius: 4,
        shadowColor: 'black',
        shadowRadius: 4,
        shadowOpacity: 0.3,
        shadowOffset: {width: 0, height: 0},
        elevation: 4,
        zIndex: 1,
    },
    body: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center'
    },
    address: {
        flex: 1,
        marginLeft: 10
    },
    selected: {
        paddingHorizontal: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 20
    },
    fnBtn: {
        height: 35,
        width: 85,
        borderColor: '#f26f21',
        borderWidth: 1,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
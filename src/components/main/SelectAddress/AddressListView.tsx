import * as React from 'react'
import { ListView, View, DeviceEventEmitter, Alert } from 'react-native'

import { connect } from 'react-redux'

import AddButton from '../../common/UIAddButton'
import SingleAddressItem from './SingleAddressItem'

import { deleteAddress } from '../../../redux/actions/index'

interface Props {
    onAddButtonPress: any
    onEditBtnPress: any
    onRowPress: any
    deleteAddress: any
    addresses: object
}

interface State {
    dataSource: any
    selectedRow: number
}

function mapStateToProps(state) {return {addresses: state.addresses.addresses}}
function mapDispatchToProps(dispatch) {
    return {
        deleteAddress: (index) => {
            dispatch(deleteAddress(index))
        }
    }
}
class AddressListView extends React.Component<Props, State> {
    constructor(props) {
        super(props)

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.selected !== r2.selected})
        this.state = {
            dataSource: ds.cloneWithRows(this.props.addresses),
            selectedRow: null
        }
    }

    componentWillMount() {
        DeviceEventEmitter.addListener('addedAddress', () => this._refreshList())
    }

    _refreshList() {
        this.setState({
            dataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1.selected !== r2.selected
            }).cloneWithRows(this.props.addresses)
        })
    }

    rowPressed(rowID) {
        const { selectedRow } = this.state
        let newSelectedRow
        rowID == selectedRow ? newSelectedRow = null : newSelectedRow = rowID
        {this.props.onRowPress(newSelectedRow)}
        this.setState({
            dataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1.selected !== r2.selected
            }).cloneWithRows(this.props.addresses),
            selectedRow: newSelectedRow
        })
    }

    _renderRow(data, _, rowID) {
        const address = `${data.address}, ${data.city}, ${data.state} ${data.zip}, ${data.country}`
        return (
            <SingleAddressItem
                address={address}
                onPress={() => this.rowPressed(rowID)}
                selected={this.state.selectedRow == rowID}
                onEditBtnPress={this.props.onEditBtnPress}
                onDeleteBtnPress={() => this._sendDeleteAlert(address)}
            ></SingleAddressItem>
        )
    }

    _sendDeleteAlert(name) {
        return Alert.alert(
            'Delete Address?',
            `'${name}' will be deleted`,
            [
                {text: 'Cancel', onPress: () => {}, style: 'cancel'},
                {text: 'Ok', onPress: () => this._deleteRow(), style: 'default'},
            ],
            {cancelable: true}
        )
    }

    _deleteRow() {
        this.props.deleteAddress(this.state.selectedRow)
        this._refreshList()
    }


    render() {
        return (
                <ListView
                    scrollEnabled={true}
                    style={{borderRadius: 4}}
                    dataSource={this.state.dataSource}
                    renderRow={this._renderRow.bind(this)}
                    renderFooter={() => {
                        return (
                            <View style={{alignItems: 'center'}}>
                                <AddButton
                                    onPress={this.props.onAddButtonPress}
                                ></AddButton>
                            </View>
                        )
                    }}
                >
                </ListView>
        )
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddressListView)
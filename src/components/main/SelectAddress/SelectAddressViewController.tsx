import * as React from 'react'
import { View, DeviceEventEmitter } from 'react-native'

import AddressListView from './AddressListView'
import { BasicHeader } from '../../common/Settings'
import FooterButton from '../../common/UIFooterButton'

import { NavigationAction, NavigationScreenProp } from 'react-navigation'

interface Props {
    navigation: NavigationScreenProp<any, NavigationAction>
}

interface State {
    selectedRow: number
}

export default class SelectAddressViewController extends React.Component<Props, State> {
    constructor(props) {
        super(props)
        this.state = {selectedRow: null}
    }
    static navigationOptions = ( {navigation} ) => BasicHeader(navigation, 'Address Selection')

    _getSelectedRow(row) {
        this.setState({selectedRow: row})
    }

    _handleSelection() {
        if (this.state.selectedRow) {
            const {goBack} = this.props.navigation
            goBack()
            DeviceEventEmitter.emit('addressSelected', {selected: this.state.selectedRow})
        } else {
            console.log('Please select an address')
        }
    }


    render() {
        const { navigate } = this.props.navigation
        return (
            <View style={{height: '100%'}}>
                <AddressListView
                    onAddButtonPress={() => navigate('AddAddress')}
                    onEditBtnPress={() => navigate('AddAddress', {edit: this.state.selectedRow})}
                    onRowPress={this._getSelectedRow.bind(this)}
                ></AddressListView>
                <FooterButton
                    title="Select"
                    onPress={() => this._handleSelection()}
                ></FooterButton>
            </View>
        )
    }
}
import ReduxThunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'
import AppReducer from './reducers'

const store = createStore(AppReducer, {}, applyMiddleware(ReduxThunk))

export default store
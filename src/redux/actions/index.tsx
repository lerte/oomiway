import {
    ADD_PAYMENT_METHOD,
    EDIT_PAYMENT_METHOD,
    ADD_TO_CART,
    REMOVE_FROM_CART,
    WITHDRAW_FROM_WALLET,
    DEL_PAYMENT_METHOD,
    ADD_ADDRESS,
    EDIT_ADDRESS,
    DEL_ADDRESS, SET_START_DATE, SET_END_DATE, SET_CART_AMOUNT
} from '../ActionTypes'

export const addToCart = (productId: number) => ({
    type: ADD_TO_CART,
    productId
})
export const removeFromCart = (productId: number) => ({
    type: REMOVE_FROM_CART,
    productId
})
export const setCartAmount = (productId: number, newQuantity: number) => ({
    type: SET_CART_AMOUNT,
    productId,
    newQuantity
})

export const withdrawFromWallet = (amount: number) => ({
    type: WITHDRAW_FROM_WALLET,
    amount
})

export const addPaymentMethod = paymentMethod => ({
    type: ADD_PAYMENT_METHOD,
    paymentMethod
})
export const editPaymentMethod = (paymentMethod, index) => ({
    type: EDIT_PAYMENT_METHOD,
    paymentMethod,
    index
})
export const deletePaymentMethod = index => ({
    type: DEL_PAYMENT_METHOD,
    index
})

export const addAddress = address => ({
    type: ADD_ADDRESS,
    address
})
export const editAddress = (address, index) => ({
    type: EDIT_ADDRESS,
    address,
    index
})
export const deleteAddress = index => ({
    type: DEL_ADDRESS,
    index
})

export const setStartDate = date => ({
    type: SET_START_DATE,
    date
})
export const setEndDate = date => ({
    type: SET_END_DATE,
    date
})
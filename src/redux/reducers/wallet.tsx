import { WITHDRAW_FROM_WALLET } from '../ActionTypes'

const initialState = {
    walletAmount: 10000
}

export default (state = initialState, action) => {
    switch (action.type) {
        case WITHDRAW_FROM_WALLET:
            return {walletAmount: state['walletAmount'] - action.amount}
        default:
            return state
    }
}
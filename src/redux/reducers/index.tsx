import { combineReducers } from 'redux'
import nav from './navigator'
import auth from './auth'
import cart from './cart'
import wallet from './wallet'
import paymentmethods from './paymentmethods'
import addresses from './addresses'
import transactions from './transactions'

export default combineReducers({
  nav,
  auth,
  cart,
  wallet,
  paymentmethods,
  addresses,
  transactions
})

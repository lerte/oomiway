import { ADD_TO_CART, REMOVE_FROM_CART, SET_CART_AMOUNT } from '../ActionTypes'

export interface Product {
    id: number
    name: string
    stock: number
    price: number
    desc?: string
    quantity: number
}

const products = {
    1: {id: 1, quantity: 0, price: 699, name: "Oomi Home", stock: 10, desc: 'Your Oomi begins with this Oomi Home starter kit.\nSee what’s happening at home from anywhere in the world, day or night. Control your entertainment system and lighting from Oomi Touch. Begin automating your house for comfort.'},
    2: {id: 2, quantity: 0, price: 0, name: "Cube", stock: 11},
    3: {id: 3, quantity: 0, price: 0, name: "Touch", stock: 8},
    4: {id: 4, quantity: 0, price: 50, name: "Bulb", stock: 9, desc: 'Any color. Any white. All in one perfect light.\nCreate the perfect ambiance for any occasion. Set bulb to a forest green or a songbird blue and instantly change the atmosphere of a room. Wake up with a gradual sunrise effect or relax with a warm white light.'},
    5: {id: 5, quantity: 0, price: 52, name: "Plug", stock: 15, desc: 'Make any outlet smart.\nGet wireless control of your lamps, electronics, appliances, and more. The LED ring provides real-time feedback on energy use and can even double as a night light. Use the USB port to charge your devices without taking up an outlet.'},
    6: {id: 6, quantity: 0, price: 52, name: "MultiSensor", stock: 6, desc: 'Six sensors in one.\nIncrease your home security and get notified if something suspicious happens. Use MultiSensor to automate your preferences. Have lights illuminate as it gets dark and turn off when you’re not around.'},
    7: {id: 7, quantity: 0, price: 29.95, name: "Infinity Dock", stock: 2, desc: 'Dock in style.\nPlace an extra Infinity Dock wherever you like putting your Oomi Touch. Dock will make sure that your Touch is charged and beautiful. Simply docking your Oomi Touch can trigger your favorite scene, such as bedtime.'}
}

export default (state = products, action) => {
    const { productId } = action;
    switch (action.type) {
        case ADD_TO_CART:
            return {...state, [productId]: quantities(state[productId], action)}
        case REMOVE_FROM_CART:
            return {...state, [productId]: quantities(state[productId], action)}
        case SET_CART_AMOUNT:
            return {...state, [productId]: quantities(state[productId], action)}
        default:
            return state
    }
}

const quantities = (state, action) => {
    const { newQuantity } = action;
    switch(action.type) {
        case ADD_TO_CART:
            return {...state, quantity: state.quantity + 1}
        case REMOVE_FROM_CART:
            if (state.quantity != 0) {
                return {...state, quantity: state.quantity - 1}
            }
            return state
        case SET_CART_AMOUNT:
            return {...state, quantity: newQuantity}
    }
}
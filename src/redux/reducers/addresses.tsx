import {ADD_ADDRESS, EDIT_ADDRESS, DEL_ADDRESS } from '../ActionTypes'

interface State {
    addresses: Array<Address>
    default: number
}

interface Address {
    address: string
    city: string
    state: string
    zip: string
    country: string
}

const initialState: State = {
    addresses: [
        {address: '736 Gateview Ave', city: 'Albany', state: 'CA', zip: '94706', country: 'USA'},
        {address: '1600 Pennsylvania Ave', city: 'Washington DC', state: "", zip: '20500', country: 'USA'}
    ],
    default: 0
}


export default (state = initialState, action) => {
    const { address, index } = action
    switch (action.type) {
        case ADD_ADDRESS:
            state.addresses.push(address)
            return state
        case EDIT_ADDRESS:
            state.addresses[index] = address
            return state
        case DEL_ADDRESS:
            state.addresses.splice(index, 1)
            return state
        default:
            return state
    }
}
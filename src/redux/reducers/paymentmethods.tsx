import { ADD_PAYMENT_METHOD, EDIT_PAYMENT_METHOD, DEL_PAYMENT_METHOD } from '../ActionTypes'

interface State {
    paymentMethods: Array<PaymentMethod>,
    default: number
}

interface PaymentMethod {
    type?: string
    title: string
    cardNum: string
    name: string
    cvv: string
    expiry: string
}

const initialState: State = {
    paymentMethods: [
        {type: "mastercard", title: 'Bernard\'s Mastercard', cardNum: '1111111111111111', name: 'Bernard Shaw', cvv: '111', expiry: '09/18'},
        {type: "visa", title: 'Lisa\'s Visa', cardNum: '2222222222222222', name: 'Lisa Simpson', cvv: '222', expiry: '01/20'}
    ],
    default: 0
}


export default (state = initialState, action) => {
    const { paymentMethod, index } = action
    switch (action.type) {
        case ADD_PAYMENT_METHOD:
            state.paymentMethods.push(paymentMethod)
            return state
        case EDIT_PAYMENT_METHOD:
            const newState = { ...state }
            newState.paymentMethods[index] = paymentMethod
            return newState
        case DEL_PAYMENT_METHOD:
            state.paymentMethods.splice(index, 1)
            return state
        default:
            return state
    }
}
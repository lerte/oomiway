import { SET_START_DATE, SET_END_DATE } from '../ActionTypes'

interface Transaction {
    category: string
    name: string
    date: string
    amount: string
    balance: string
}

interface State {
    startDate: string
    endDate: string
    transactions: Array<Transaction>
}

const initialState: State = {
    startDate: '',
    endDate: '',
    transactions: [
        {
            category: "sale",
            name: 'Sale',
            date: '2017-11-30',
            amount: '$1,000',
            balance: '$5,700'
        },
        {
            category: "sale",
            name: 'Sale',
            date: '2017-11-29',
            amount: '$1,000',
            balance: '$4,700'
        },
        {
            category: "restock",
            name: 'Restock',
            date: '2017-05-26',
            amount: '$800',
            balance: '$3,700'
        },
        {
            category: "transfer",
            name: 'Transfer Out',
            date: '2017-05-23',
            amount: '$1,500',
            balance: '$4,500'
        },
        {
            category: "sale",
            name: 'Sale',
            date: '2017-04-19',
            amount: '$1,800',
            balance: '$3,000'
        },
        {
            category: "sale",
            name: 'Sale',
            date: '2017-04-07',
            amount: '$1,200',
            balance: '$1,200'
        }
    ]
}

export default (state = initialState, action) => {
    const { date } = action
    switch (action.type) {
        case SET_START_DATE:
            return {...state, startDate: date}
        case SET_END_DATE:
            return {...state, endDate: date}
        default:
            return state
    }
}
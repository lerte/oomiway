import { connect } from 'react-redux'
import { createAppContainer, createStackNavigator } from 'react-navigation'

import LoginViewController from '../components/account/LoginViewController'
import SignUpViewController from '../components/account/SignUpViewController'
import RecoverPasswordViewController from '../components/account/RecoverPasswordViewController'

import HomeViewController from '../components/main/Home/HomeViewController'
import ProductsViewController from '../components/main/Products/ProductsViewController'
import ProductDetailViewController from '../components/main/ProductDetail/ProductDetailViewController'
import MyCartViewController from '../components/main/MyCart/MyCartViewController'
import OrderConfirmViewController from '../components/main/OrderConfirm/OrderConfirmViewController'
import PaymentMethodsViewController from '../components/main/PaymentMethods/PaymentMethodsViewController'
import SelectAddressViewController from '../components/main/SelectAddress/SelectAddressViewController'
import PurchaseCompleteViewController from '../components/main/PurchaseComplete/PurchaseCompleteViewController'

import ScannerViewController from '../components/sell/Scanner/ScannerViewController'
import ReceiveMoneyViewController from '../components/sell/ReceiveMoneyViewController'
import PaymentReceivedViewController from '../components/sell/PaymentReceived/PaymentReceivedViewController'

import AccountProfileViewController from '../components/profile/AccountProfile/AccountProfileViewController'
import ModifyEmailAddressController from '../components/profile/ModifyEmailAddressController'
import UpdatePaymentMethodsViewController from '../components/profile/UpdatePaymentMethodsViewController'
import AddCreditCardViewController from '../components/profile/AddCreditCardViewController'
import UpdateAccountInfoViewController from '../components/profile/UpdateAccountInfo/UpdateAccountinfoViewController'
import AddressListViewController from '../components/profile/AddressListViewController'
import AddAddressViewController from '../components/profile/AddAddressViewController'

import TransactionsTabViewController from '../components/transactions/TransactionsTabView/TransactionsTabViewController'
import CalendarFilterViewController from '../components/transactions/CalendarFilter/CalendarFilterViewController'
import SalesFiltedByDateViewController from '../components/transactions/SalesFiltedByDateViewController'

import WalletMainViewController from '../components/wallet/WalletMain/WalletMainViewController'
import WithdrawViewController from '../components/wallet/Withdraw/WithdrawViewController'
import WithdrawSuccessfulViewController from '../components/wallet/WithdrawSuccessful/WithdrawSuccessfulViewController'

export const AppNavigator = createStackNavigator({
	//account
	Login: {screen: LoginViewController},
	SignUp: {screen: SignUpViewController},
	RecoverPassword: {screen: RecoverPasswordViewController},

	//main
	Home: {screen: HomeViewController},
	Products: {screen: ProductsViewController},
	ProductDetail: {screen: ProductDetailViewController},
	MyCart: {screen: MyCartViewController, navigationOptions: {headerTitle: 'My Cart'}},
	OrderConfirm: {screen: OrderConfirmViewController},
	PaymentMethods: {screen: PaymentMethodsViewController, navigationOptions: {headerTitle: 'Payment Methods'}},
	SelectAddress: {screen: SelectAddressViewController},
	PurchaseComplete: {screen: PurchaseCompleteViewController},

	//sell
	Scanner: {screen: ScannerViewController, navigationOptions: {headerTitle: 'Scan'}},
	ReceiveMoney: {screen: ReceiveMoneyViewController},
	PaymentReceived: {screen: PaymentReceivedViewController},

	//profile
	AccountProfile: {screen: AccountProfileViewController},
	ModifyEmailAddress: {screen: ModifyEmailAddressController},
	UpdatePaymentMethods: {screen: UpdatePaymentMethodsViewController},
	AddCreditCard: {screen: AddCreditCardViewController},
	UpdateAccountInfo: {screen: UpdateAccountInfoViewController},
	AddressList: {screen: AddressListViewController},
	AddAddress: {screen: AddAddressViewController},

	//transactions
	Transactions: {screen: TransactionsTabViewController},
	CalendarFilter: {screen: CalendarFilterViewController},
	SalesFiltedByDate: {screen: SalesFiltedByDateViewController},

	//wallet
	WalletMain: {screen: WalletMainViewController},
	Withdraw: {screen: WithdrawViewController},
	WithdrawSuccessful: {screen: WithdrawSuccessfulViewController}
},{
	initialRouteName: 'Home'
})

const mapStateToProps = state => ({
	nav: state.nav
})
const AppWithNavigationState = createAppContainer(AppNavigator)

export default connect(mapStateToProps)(AppWithNavigationState)
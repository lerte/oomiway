# OomiWay App

## Getting Started
1. Ensure you've followed the [React Native - Get Started Guide](https://facebook.github.io/react-native/docs/getting-started.html)
2. Run `yarn` from root directory
3. Start the app in simulator or device
    - `npm run start` for start react-native packager service
    - `npm run build` for build .tsx files to .js
    - `npm run android/ios`
4. `npm run app-icon` for generate app icons

## Stack
- [React Native](https://facebook.github.io/react-native/) for building mobile apps using Javascript
- [Redux](http://rackt.github.io/redux/index.html) a predictable state container for Javascript apps
- [Redux Thunk](https://github.com/gaearon/redux-thunk) middleware for Redux
- [TypeScript](https://www.typescriptlang.org/) for TypeScript support
- [React Navigation](https://reactnavigation.org/) navigation for React Native

## File Structure
- `/android` - Android native stuff
- `/ios` - iOS native stuff
- `/src` - Contains our React Native App codebase
  - `actions` - Actions
  - `/assets` - Images, Fonts and others
  - `/components` - Dumb components
    - `/common` - Shared components
  - `/config` - Config files
  - `/reducers` - Reducers
  - `/App.js` - Base component
  - `/Router.js` - App navigation
  - `/Store.js` - Store